package com.trambambula.db.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.time.LocalDateTime;

@MappedSuperclass
@Data
public abstract class AbstractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "last_modification_date")
    private LocalDateTime lastModificationDate = LocalDateTime.now();

    private Long version = 1L;

    @Override
    public String toString() {
        return getClass() + "{id=" + id + '}';
    }

    protected static Long getIdOrNull(final AbstractEntity entity) {
        return entity == null ? null : entity.id;
    }
}
