package com.trambambula.db.model.user;

public enum UserRole {
    ADMIN,
    USER;
}
