package com.trambambula.db.model.tournament.system;

import com.trambambula.db.model.AbstractEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)

@Entity
@Table(name = "tournament_systems_tbl")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.STRING, length = 20)
@SuppressWarnings("checkstyle:magicnumber")
public abstract class TournamentSystemEntity extends AbstractEntity {

    @Column(length = 128)
    private String name;
}
