package com.trambambula.db.model.tournament;

public enum TournamentStatus {
    NEW,
    STARTED,
    COMPLETED
}
