package com.trambambula.db.model.tournament;

public enum TournamentType {
    GROUP,
    INDIVIDUAL
}
