package com.trambambula.db.dao;

import com.trambambula.db.model.user.UserEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {

    @Transactional(readOnly = true)
    Optional<UserEntity> findUserByEmailIgnoreCase(String email);

    @Transactional(readOnly = true)
    @Query("from UserEntity u where concat(lower(u.email), lower(u.firstName), lower(u.lastName)) like :query")
    Page<UserEntity> search(@Param("query") String query, Pageable pageable);

    @Transactional(readOnly = true)
    @Query("from UserEntity u where concat(lower(u.firstName), lower(u.lastName)) like :query")
    Page<UserEntity> searchByUserName(@Param("query") String query, Pageable pageable);
}
