package com.trambambula.db.model.tournament.match;

import com.trambambula.db.model.AbstractEntity;
import com.trambambula.db.model.tournament.TournamentParticipantEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.mapstruct.ap.internal.util.Strings;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.Set;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)

@Entity
@Table(name = "tournament_matches_tbl")
@SuppressWarnings("checkstyle:magicnumber")
public class TournamentMatchEntity extends AbstractEntity {

    @ManyToMany
    @JoinTable(
            name = "tournament_match_hosts_tbl",
            inverseJoinColumns = @JoinColumn(name = "participant_id"),
            joinColumns = @JoinColumn(name = "match_id")
    )
    private Set<TournamentParticipantEntity> hosts;

    @ManyToMany
    @JoinTable(
            name = "tournament_match_guests_tbl",
            inverseJoinColumns = @JoinColumn(name = "participant_id"),
            joinColumns = @JoinColumn(name = "match_id")
    )
    private Set<TournamentParticipantEntity> guests;

    @Column(name = "host_points")
    private Integer hostPoints;

    @Column(name = "guest_points")
    private Integer guestPoints;

    @Column(name = "date_time")
    private LocalDateTime dateTime;

    @Column(length = 128)
    private String description;

    public boolean isCompleted() {
        return dateTime != null;
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("TournamentMatchEntity{id=").append(getId());

        if (hosts != null && guests != null) {
            builder.append(", hosts=[");
            builder.append(Strings.join(hosts, ",", TournamentParticipantEntity::getDisplayName));
            builder.append("], guests=[");
            builder.append(Strings.join(guests, ",", TournamentParticipantEntity::getDisplayName));
            builder.append(']');
        }

        return builder.append('}').toString();
    }
}
