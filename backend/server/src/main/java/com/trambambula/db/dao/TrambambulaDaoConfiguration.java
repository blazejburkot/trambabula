package com.trambambula.db.dao;

import com.trambambula.db.model.AbstractEntity;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EntityScan(basePackageClasses = AbstractEntity.class)
@EnableTransactionManagement
@EnableJpaRepositories(basePackageClasses = UserRepository.class)
public class TrambambulaDaoConfiguration {
}
