package com.trambambula.db.model.user;

import com.trambambula.db.model.AbstractEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true, exclude = "password")

@Entity
@Table(name = "users_tbl")
@DynamicUpdate
@SuppressWarnings("checkstyle:magicnumber")
public class UserEntity extends AbstractEntity {

    @Column(length = 128, nullable = false)
    private String email;

    @Column(length = 256, nullable = false)
    private String password;

    @Column(name = "first_name", length = 128)
    private String firstName;

    @Column(name = "last_name", length = 128)
    private String lastName;

    @Column(name = "default_display_name", length = 128)
    private String defaultDisplayName;

    @Enumerated(EnumType.STRING)
    @Column(length = 10)
    private UserRole role;
}
