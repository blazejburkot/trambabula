package com.trambambula.db.model.tournament.match;

import com.trambambula.db.model.AbstractEntity;
import com.trambambula.db.model.tournament.system.TournamentSystemEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)

@Entity
@Table(name = "tournament_stages_tbl")
public class TournamentStageEntity extends AbstractEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tournament_system_id")
    private TournamentSystemEntity tournamentSystemEntity;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "tournament_stage_id", nullable = false)
    private Set<TournamentMatchEntity> matches;
}
