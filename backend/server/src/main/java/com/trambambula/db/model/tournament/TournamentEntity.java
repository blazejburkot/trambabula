package com.trambambula.db.model.tournament;

import com.trambambula.db.model.AbstractEntity;
import com.trambambula.db.model.tournament.system.TournamentSystemEntity;
import com.trambambula.db.model.user.UserEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.Set;


@Getter
@Setter
@EqualsAndHashCode(callSuper = true)

@Entity
@Table(name = "tournaments_tbl")
@DynamicUpdate
@SuppressWarnings("checkstyle:magicnumber")
public class TournamentEntity extends AbstractEntity {

    @Column(length = 128, nullable = false)
    private String name;

    @Column(length = 128)
    private String url;

    @Column(name = "start_date")
    private LocalDateTime startDate;

    @Column(name = "end_date")
    private LocalDateTime endDate;

    @Enumerated(EnumType.STRING)
    @Column(length = 20, nullable = false)
    private TournamentType type;

    @Enumerated(EnumType.STRING)
    @Column(length = 20, nullable = false)
    private TournamentStatus status;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "tournament_system_id", nullable = false)
    private TournamentSystemEntity system;

    @Enumerated(EnumType.STRING)
    @Column(length = 20, nullable = false)
    private TournamentVisibility visibility;

    @ManyToOne
    @JoinColumn(name = "owner_id", nullable = false)
    private UserEntity owner;

    @OneToMany(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "tournament_id", nullable = false)
    private Set<TournamentParticipantEntity> participants;
}
