package com.trambambula.db.model.tournament;

import com.trambambula.db.model.AbstractEntity;
import com.trambambula.db.model.user.UserEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)

@Entity
@Table(name = "tournament_participants_tbl")
@DynamicUpdate
@SuppressWarnings("checkstyle:magicnumber")
public class TournamentParticipantEntity extends AbstractEntity {

    @Column(name = "display_name", length = 128, nullable = false)
    private String displayName;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserEntity user;

    @Override
    public String toString() {
        return "TournamentParticipantEntity{"
                + "displayName='" + displayName + '\''
                + ", user.id=" + getIdOrNull(user)
                + ", id=" + getId()
                + '}';
    }
}
