package com.trambambula.db.model.tournament.system;

import com.trambambula.db.model.tournament.match.TournamentStageEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)

@Entity
@DiscriminatorValue("TRAMBAMBULA")
public class TrambambulaSystemEntity extends TournamentSystemEntity {

    @OneToOne(cascade = CascadeType.ALL, optional = true, mappedBy = "tournamentSystemEntity")
    private TournamentStageEntity stage;
}
