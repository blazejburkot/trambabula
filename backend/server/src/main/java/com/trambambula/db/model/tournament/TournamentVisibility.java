package com.trambambula.db.model.tournament;

public enum TournamentVisibility {
    PUBLIC,
    PUBLIC_URL,
    PRIVATE
}
