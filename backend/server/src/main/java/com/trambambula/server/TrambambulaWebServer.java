package com.trambambula.server;

import com.trambambula.db.dao.TrambambulaDaoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(TrambambulaDaoConfiguration.class)
@SuppressWarnings("checkstyle:hideutilityclassconstructor")
public class TrambambulaWebServer {

    public static void main(final String[] args) {
        SpringApplication.run(TrambambulaWebServer.class, args);
    }
}
