package com.trambambula.server.core.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Optional;

public final class SessionUtils {
    private SessionUtils() {
    }

    public static Optional<HttpSession> getSession(final HttpServletRequest httpServletRequest) {
        return Optional.ofNullable(httpServletRequest.getSession(false));
    }
}
