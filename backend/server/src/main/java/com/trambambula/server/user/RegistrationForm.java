package com.trambambula.server.user;

import io.swagger.annotations.ApiParam;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotBlank;

@Data
@ToString(exclude = "password")
class RegistrationForm {

    @ApiParam
    private String firstName = "";

    @ApiParam
    private String lastName = "";

    @ApiParam(required = true)
    @NotBlank
    private String email;

    @ApiParam(required = true)
    @NotBlank
    private String password;
}
