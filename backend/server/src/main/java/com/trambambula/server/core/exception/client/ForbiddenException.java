package com.trambambula.server.core.exception.client;

import org.springframework.http.HttpStatus;

@SuppressWarnings("squid:MaximumInheritanceDepth") // suppress sonar issue: Inheritance tree of classes should not be too deep
public class ForbiddenException extends ClientBaseException {

    public ForbiddenException(final String message) {
        super(message);
    }

    public ForbiddenException(final String message, final Exception ex) {
        super(message, ex);
    }

    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.FORBIDDEN;
    }
}
