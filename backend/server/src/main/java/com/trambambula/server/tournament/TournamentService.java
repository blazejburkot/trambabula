package com.trambambula.server.tournament;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.Lists;
import com.trambambula.db.dao.TournamentRepository;
import com.trambambula.db.model.tournament.TournamentEntity;
import com.trambambula.db.model.tournament.TournamentParticipantEntity;
import com.trambambula.db.model.tournament.match.TournamentMatchEntity;
import com.trambambula.db.model.tournament.match.TournamentStageEntity;
import com.trambambula.db.model.tournament.system.TournamentSystemEntity;
import com.trambambula.db.model.tournament.system.TrambambulaSystemEntity;
import com.trambambula.server.core.exception.client.NotFoundException;
import com.trambambula.server.core.security.model.UserPrincipal;
import com.trambambula.server.tournament.dto.CreateTournamentRequest;
import com.trambambula.server.tournament.dto.TournamentDetails;
import com.trambambula.server.tournament.dto.TournamentOverviewData;
import com.trambambula.server.tournament.dto.TournamentSearchRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Log4j2
@Service
@RequiredArgsConstructor
class TournamentService {
    private final RandomGenerator randomGenerator;
    private final TournamentMapperService mapper;
    private final TournamentValidator validator;
    private final TournamentRepository repository;

    TournamentDetails getById(final long id) {
        final TournamentEntity tournamentEntity = getTournamentEntity(id);
        return mapper.mapDetails(tournamentEntity);
    }

    public List<TournamentOverviewData> search(final Principal principal, final TournamentSearchRequest searchRequest) {
        final List<TournamentEntity> tournamentEntities = repository.findAll(); // TODO restrict loaded data from DB

        return tournamentEntities.isEmpty()
                ? Collections.emptyList()
                : Lists.transform(tournamentEntities, mapper::mapBaseInformation);
    }

    private TournamentEntity getTournamentEntity(final long id) {
        return repository.findById(id)
                .orElseThrow(() -> new NotFoundException("Tournament not found: " + id));
    }

    public long createTournament(final UserPrincipal creator, final CreateTournamentRequest request) {
        validator.validateCreateTournamentRequest(request);

        final TournamentEntity entity = mapper.mapToEntity(creator.getId(), request);

        if (StringUtils.isEmpty(request.getUrl())) {
            entity.setUrl(randomGenerator.randomUrl());
        }

        final TournamentSystemEntity tournamentSystem = new TrambambulaSystemEntity();
        entity.setSystem(tournamentSystem);

        final TournamentEntity persistedEntity = repository.save(entity);

        return persistedEntity.getId();
    }

    @VisibleForTesting
    @SuppressWarnings("checkstyle:magicnumber")
    TrambambulaSystemEntity createTrambambulaTournament(final Collection<TournamentParticipantEntity> participants) {
        final TrambambulaSystemEntity system = new TrambambulaSystemEntity();
        final TournamentStageEntity stage = new TournamentStageEntity();
        system.setStage(stage);
        stage.setTournamentSystemEntity(system);
        stage.setMatches(new HashSet<>());

        final ArrayList<TournamentParticipantEntity> participantsList = new ArrayList<>(participants);
        final int size = participantsList.size();
        for (int part1 = 0; part1 < size - 3; ++part1) {
            for (int part2 = part1 + 1; part2 < size; ++part2) {
                for (int part3 = part1 + 1; part3 < size; ++part3) {
                    if (part3 == part2) {
                        continue;
                    }

                    for (int part4 = part3 + 1; part4 < size; ++part4) {
                        if (part4 == part2 || part4 == part3) {
                            continue;
                        }

                        final Set<TournamentParticipantEntity> pair1 = Set.of(participantsList.get(part1), participantsList.get(part2));
                        final Set<TournamentParticipantEntity> pair2 = Set.of(participantsList.get(part3), participantsList.get(part4));

                        final TournamentMatchEntity match = new TournamentMatchEntity();
                        if (randomGenerator.randomBoolean()) {
                            match.setHosts(pair1);
                            match.setGuests(pair2);
                        } else {
                            match.setHosts(pair2);
                            match.setGuests(pair1);
                        }

                        stage.getMatches().add(match);
                    }
                }
            }
        }

        return system;
    }
}
