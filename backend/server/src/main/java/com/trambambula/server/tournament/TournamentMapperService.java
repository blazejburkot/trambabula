package com.trambambula.server.tournament;

import com.trambambula.db.model.tournament.TournamentEntity;
import com.trambambula.db.model.tournament.TournamentStatus;
import com.trambambula.db.model.tournament.match.TournamentMatchEntity;
import com.trambambula.db.model.tournament.match.TournamentStageEntity;
import com.trambambula.db.model.tournament.system.TournamentSystemEntity;
import com.trambambula.db.model.tournament.system.TrambambulaSystemEntity;
import com.trambambula.db.model.user.UserEntity;
import com.trambambula.server.tournament.dto.CreateTournamentRequest;
import com.trambambula.server.tournament.dto.TournamentDetails;
import com.trambambula.server.tournament.dto.TournamentMatch;
import com.trambambula.server.tournament.dto.TournamentOverviewData;
import com.trambambula.server.tournament.dto.TournamentStage;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
class TournamentMapperService {
    private final TournamentMapper tournamentMapper = TournamentMapper.INSTANCE;

    TournamentOverviewData mapBaseInformation(final TournamentEntity tournamentEntity) {
        return tournamentMapper.mapBaseInformation(tournamentEntity);
    }

    TournamentDetails mapDetails(final TournamentEntity tournamentEntity) {
        final TournamentDetails tournament = tournamentMapper.mapDetails(tournamentEntity);

        if (tournamentEntity.getStatus() != TournamentStatus.NEW) {
            tournament.setStages(mapTournamentStages(tournamentEntity.getSystem()));
        }

        return tournament;
    }

    TournamentEntity mapToEntity(final long creatorId, final CreateTournamentRequest request) {
        final TournamentEntity entity = tournamentMapper.mapToEntity(request);

        final UserEntity userEntity = new UserEntity();
        userEntity.setId(creatorId);
        entity.setOwner(userEntity);

        return entity;
    }

    private List<TournamentStage> mapTournamentStages(final TournamentSystemEntity tournamentSystem) {
        if (tournamentSystem instanceof TrambambulaSystemEntity) {
            return List.of(mapTournamentStage(((TrambambulaSystemEntity) tournamentSystem).getStage()));
        }

        throw new IllegalArgumentException("Unsupported Tournament System: " + tournamentSystem);
    }

    private TournamentStage mapTournamentStage(final TournamentStageEntity entity) {
        final TournamentStage stage = new TournamentStage();
        stage.setId(entity.getId());

        for (TournamentMatchEntity matchEntity : entity.getMatches()) {
            final TournamentMatch tournamentMatch = tournamentMapper.mapMatchData(matchEntity);
            if (matchEntity.isCompleted()) {
                stage.getMatchesCompleted().add(tournamentMatch);
            } else {
                stage.getMatchesToBePlayed().add(tournamentMatch);
            }
        }


        return stage;
    }
}
