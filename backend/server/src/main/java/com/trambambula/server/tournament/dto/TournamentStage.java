package com.trambambula.server.tournament.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class TournamentStage {
    private long id;
    private List<TournamentMatch> matchesToBePlayed = new ArrayList<>();
    private List<TournamentMatch> matchesCompleted = new ArrayList<>();
}
