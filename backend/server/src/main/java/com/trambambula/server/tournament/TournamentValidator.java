package com.trambambula.server.tournament;

import com.trambambula.db.model.tournament.TournamentType;
import com.trambambula.server.core.exception.client.BadRequestException;
import com.trambambula.server.tournament.dto.CreateTournamentRequest;
import com.trambambula.server.tournament.dto.TournamentParticipant;
import com.trambambula.server.tournament.dto.TournamentSystem;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Component
class TournamentValidator {
    private static final Pattern URL_REGEX = Pattern.compile("[\\w-]+");

    public void validateCreateTournamentRequest(final CreateTournamentRequest request) {
        final TournamentSystem system = request.getSystem();
        final List<TournamentParticipant> participants = request.getParticipants();

        checkNumberOfParticipants(system, participants);
        givenSystemShouldBeAllowedForType(system, request.getType());
        participantsShouldHaveUniqueNames(participants);
        usersShouldBeUnique(participants);
        validateUrl(request.getUrl());
    }

    private void validateUrl(final String url) {
        if (StringUtils.hasText(url)) {
            assertTrue(URL_REGEX.matcher(url).matches(), "ILLEGAL_CHARACTERS_IN_URL");
            // TODO Issue #21 url should be unique
        }
    }

    private void checkNumberOfParticipants(final TournamentSystem system, final List<TournamentParticipant> participants) {
        assertTrue(participants.size() >= system.getMinimalNumberOfParticipants(), "TOO_FEW_PARTICIPANTS");
    }

    private void usersShouldBeUnique(final List<TournamentParticipant> participants) {
        final Set<Long> userIds = new HashSet<>();
        for (TournamentParticipant participant : participants) {
            final Long userId = participant.getUserId();
            assertTrue(userId == null || !userIds.contains(userId), "USER_CANNOT_BE_ASSIGNED_TO_TWO_PARTICIPANTS");
            userIds.add(userId);
        }
    }

    private void participantsShouldHaveUniqueNames(final Collection<TournamentParticipant> participants) {
        final Set<String> result = participants.stream()
                .map(TournamentParticipant::getDisplayName)
                .map(String::toLowerCase)
                .collect(Collectors.toSet());
        assertTrue(result.size() == participants.size(), "DISPLAY_NAMES_ARE_NOT_UNIQUES");
    }

    private void givenSystemShouldBeAllowedForType(final TournamentSystem system, final TournamentType type) {
        assertTrue(system.getEnabledForTypes().contains(type), "INCORRECT_TYPE_FOR_SYSTEM");
    }

    private void assertTrue(final boolean condition, final String errorCode) {
        if (!condition) {
            throw new BadRequestException(errorCode);
        }
    }
}
