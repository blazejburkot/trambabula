package com.trambambula.server.core.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
class ExceptionResponse {
    private final long timestamp;
    private final int status;
    private final String error;
    private final String message;
    private final String url;
    private final String method;

    ExceptionResponse(final Throwable ex, final String method, final String url, final HttpStatus status) {
        this.method = method;
        this.timestamp = System.currentTimeMillis();
        this.error = ex.getClass().getSimpleName();
        this.message = ex.getMessage();
        this.url = url;
        this.status = status.value();
    }
}
