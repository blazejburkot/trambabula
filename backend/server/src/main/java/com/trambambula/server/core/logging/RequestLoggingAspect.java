package com.trambambula.server.core.logging;

import lombok.extern.log4j.Log4j2;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Log4j2
@Aspect
@Component
public class RequestLoggingAspect {
    private final AspectHelper aspectHelper;

    RequestLoggingAspect(final AspectHelper aspectHelper) {
        this.aspectHelper = aspectHelper;
    }

    @Pointcut("@annotation(org.springframework.web.bind.annotation.RequestMapping)"
            + "|| @annotation(org.springframework.web.bind.annotation.GetMapping)"
            + "|| @annotation(org.springframework.web.bind.annotation.PostMapping)"
            + "|| @annotation(org.springframework.web.bind.annotation.DeleteMapping)"
            + "|| @annotation(org.springframework.web.bind.annotation.PutMapping)"
            + "|| @annotation(org.springframework.web.bind.annotation.PatchMapping)")
    void anyRestMethod() {
    }

    @Pointcut("@within(org.springframework.web.bind.annotation.RestController)"
            + "|| @within(org.springframework.stereotype.Controller)"
            + "|| @within(org.springframework.web.bind.annotation.ResponseBody)"
            + "|| @within(org.springframework.web.bind.annotation.RequestMapping)")
    void withinController() {
    }

    @Pointcut("within(com.trambambula..*)")
    void withinProjectPackage() {
    }

    @Pointcut("anyRestMethod() && withinController() && withinProjectPackage()")
    void anyRestMethodWithinProjectController() {
    }


    @Before("anyRestMethodWithinProjectController()")
    public void before(final JoinPoint joinPoint) {
        log.info("{} args: {}", aspectHelper.getMethodName(joinPoint), aspectHelper.getArguments(joinPoint));
    }

    @AfterReturning(pointcut = "anyRestMethodWithinProjectController()", returning = "result")
    public void after(final JoinPoint joinPoint, final Object result) {
        log.info("{} return: {}({})", aspectHelper.getMethodName(joinPoint), getType(result), result);
    }

    @AfterThrowing(pointcut = "anyRestMethodWithinProjectController()", throwing = "ex")
    public void after(final JoinPoint joinPoint, final Throwable ex) {
        log.info("{} throw: {}({})", aspectHelper.getMethodName(joinPoint), getType(ex), getMessage(ex));
    }


    private String getType(final Object result) {
        return result == null ? "Null" : result.getClass().getSimpleName();
    }

    private String getMessage(final Throwable ex) {
        return ex == null ? "[ex is null]" : ex.getMessage();
    }
}
