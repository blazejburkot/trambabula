package com.trambambula.server.core.exception.client;

import com.trambambula.server.core.exception.HttpBaseException;

/**
 * 4xx Client errors
 * Base class for exceptions thrown by this web-application
 * whenever the error seems to have been caused by the client.
 */
public abstract class ClientBaseException extends HttpBaseException {

    ClientBaseException(final String message) {
        super(message);
    }

    ClientBaseException(final String message, final Exception ex) {
        super(message, ex);
    }

    ClientBaseException(final Exception ex) {
        super(ex);
    }
}
