package com.trambambula.server.core.exception.client;

import org.springframework.http.HttpStatus;

@SuppressWarnings("squid:MaximumInheritanceDepth") // suppress sonar issue: Inheritance tree of classes should not be too deep
public class ConflictException extends ClientBaseException {

    public ConflictException(final Exception ex) {
        super(ex);
    }

    public ConflictException(final String message) {
        super(message);
    }

    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.CONFLICT;
    }
}
