package com.trambambula.server.core.util;

import com.trambambula.server.core.security.model.UserPrincipal;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Optional;

public final class SecurityUtils {
    private SecurityUtils() {
    }

    public static Optional<UserPrincipal> getPrincipal() {
        return Optional.ofNullable(SecurityContextHolder.getContext().getAuthentication())
            .filter(Authentication::isAuthenticated)
            .map(Authentication::getPrincipal)
            .filter(UserPrincipal.class::isInstance)
            .map(UserPrincipal.class::cast);
    }
}
