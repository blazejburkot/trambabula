package com.trambambula.server.tournament;

import com.trambambula.server.core.security.model.UserPrincipal;
import com.trambambula.server.tournament.dto.CreateTournamentRequest;
import com.trambambula.server.tournament.dto.TournamentDetails;
import com.trambambula.server.tournament.dto.TournamentOverviewData;
import com.trambambula.server.tournament.dto.TournamentSearchRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@Log4j2
@RestController
@RequestMapping(value = "/api/v1/tournaments", produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
class TournamentRestController {
    private final TournamentService service;

    @GetMapping("/{id:\\d+}")
    TournamentDetails getTournament(@PathVariable final long id) {
        return service.getById(id);
    }

    @GetMapping("")
    List<TournamentOverviewData> search(final Principal principal,
                                        @RequestParam(required = false) final TournamentSearchRequest searchRequest) {
        log.debug("search principal: {}, request: {}", principal, searchRequest);
        return service.search(principal, searchRequest);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    long createTournament(@Valid @RequestBody final CreateTournamentRequest request,
                          final Authentication authentication) {
        final UserPrincipal principal = (UserPrincipal) authentication.getPrincipal();
        log.debug("request: {}, principal {}", request, principal);
        return service.createTournament(principal, request);
    }
}
