package com.trambambula.server.core.security.model;

import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.security.Principal;
import java.util.Collection;

@Getter
public class UserPrincipal extends User implements Principal {
    private final long id;

    public UserPrincipal(final long userId, final String username, final String password,
                         final Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
        this.id = userId;
    }

    @Override
    public String getName() {
        return getUsername();
    }

    @Override
    public String toString() {
        return "UserPrincipal{"
            + "id=" + id
            + ", name=" + getUsername()
            + '}';
    }
}
