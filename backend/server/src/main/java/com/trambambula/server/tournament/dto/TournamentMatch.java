package com.trambambula.server.tournament.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Set;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TournamentMatch {
    private long id;
    private Set<Long> hostIds;
    private Set<Long> guestIds;
    private Integer hostPoints;
    private Integer guestPoints;
    private LocalDateTime dateTime;
    private String description;
}
