package com.trambambula.server.core.logging;

import com.google.common.collect.Lists;
import lombok.extern.log4j.Log4j2;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Log4j2
@Component
class AspectHelper {
    private static final Collection<String> MASK_ARGUMENTS = Lists.newArrayList("password", "secret");

    @Nullable
    public String getMethodName(final JoinPoint joinPoint) {
        final Signature signature = joinPoint.getSignature();
        return isMethod(signature) ? signature.toShortString() : null;
    }

    @Nullable
    public Map<String, Object> getArguments(final JoinPoint joinPoint) {
        final Signature signature = joinPoint.getSignature();
        if (!isMethod(signature)) {
            return null;
        }

        final MethodSignature methodSignature = (MethodSignature) signature;
        final String[] argNames = methodSignature.getParameterNames();
        final Object[] argValues = joinPoint.getArgs();

        if (argNames == null || argValues == null || argNames.length != argValues.length) {
            log.warn("jointPoint {}, argNames ({}) and argValues ({}) have different length or one of them is null",
                    joinPoint, Arrays.toString(argNames), Arrays.toString(argValues));
            return null;
        }

        final Map<String, Object> args = new HashMap<>(argNames.length);
        for (int idx = 0; idx < argNames.length; ++idx) {
            args.put(argNames[idx], doMaskValue(argNames[idx]) ? "***" : argValues[idx]);
        }
        return args;
    }

    private boolean doMaskValue(final String argName) {
        for (String arg : MASK_ARGUMENTS) {
            if (StringUtils.hasText(argName) && argName.toLowerCase().contains(arg)) {
                return true;
            }
        }
        return false;
    }

    private boolean isMethod(final Signature signature) {
        return signature instanceof MethodSignature;
    }
}
