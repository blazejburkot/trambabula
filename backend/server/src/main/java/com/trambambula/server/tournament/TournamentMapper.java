package com.trambambula.server.tournament;

import com.trambambula.db.model.AbstractEntity;
import com.trambambula.db.model.tournament.TournamentEntity;
import com.trambambula.db.model.tournament.TournamentParticipantEntity;
import com.trambambula.db.model.tournament.match.TournamentMatchEntity;
import com.trambambula.db.model.tournament.system.TournamentSystemEntity;
import com.trambambula.db.model.user.UserEntity;
import com.trambambula.server.tournament.dto.CreateTournamentRequest;
import com.trambambula.server.tournament.dto.TournamentDetails;
import com.trambambula.server.tournament.dto.TournamentMatch;
import com.trambambula.server.tournament.dto.TournamentOverviewData;
import com.trambambula.server.tournament.dto.TournamentOwner;
import com.trambambula.server.tournament.dto.TournamentParticipant;
import com.trambambula.server.tournament.dto.TournamentSystem;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.factory.Mappers;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface TournamentMapper {
    TournamentMapper INSTANCE = Mappers.getMapper(TournamentMapper.class);

    @Mapping(target = "system", qualifiedByName = "systemToEnumMapper")
    TournamentOverviewData mapBaseInformation(TournamentEntity tournamentEntity);

    @Mapping(target = "stages", ignore = true)
    @Mapping(target = "system", qualifiedByName = "systemToEnumMapper")
    TournamentDetails mapDetails(TournamentEntity tournamentEntity);

    @Mapping(target = "userId", source = "user.id")
    @Mapping(target = "participantId", source = "id")
    @Mapping(target = "firstName", source = "user.firstName")
    @Mapping(target = "lastName", source = "user.lastName")
    TournamentParticipant mapParticipant(TournamentParticipantEntity entity);

    @Mapping(target = "userId", source = "id")
    TournamentOwner mapOwner(UserEntity entity);

    @Mapping(target = "hostIds", source = "hosts", qualifiedByName = "extractIds")
    @Mapping(target = "guestIds", source = "guests", qualifiedByName = "extractIds")
    TournamentMatch mapMatchData(TournamentMatchEntity entity);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "lastModificationDate", ignore = true)
    @Mapping(target = "version", ignore = true)
    @Mapping(target = "endDate", ignore = true)
    @Mapping(target = "status", constant = "NEW")
    @Mapping(target = "system", ignore = true)
    @Mapping(target = "owner", ignore = true)
    @Mapping(target = "participants", qualifiedByName = "mapParticipantToEntity")
    TournamentEntity mapToEntity(CreateTournamentRequest request);

    @Named("systemToEnumMapper")
    static TournamentSystem systemToEnumMapper(final TournamentSystemEntity system) {
        return TournamentSystem.fromTournamentSystemClass(system.getClass());
    }

    @Named("extractIds")
    static Set<Long> extractIds(final Collection<? extends AbstractEntity> entities) {
        return entities.stream()
                .map(AbstractEntity::getId)
                .collect(Collectors.toSet());
    }

    @Named("mapParticipantToEntity")
    static TournamentParticipantEntity mapParticipantToEntity(final TournamentParticipant participant) {
        final TournamentParticipantEntity entity = new TournamentParticipantEntity();
        entity.setDisplayName(participant.getDisplayName());
        if (participant.getUserId() != null) {
            entity.setUser(mapToUserEntity(participant));
        }
        return entity;
    }

    static UserEntity mapToUserEntity(final TournamentOwner owner) {
        final UserEntity userEntity = new UserEntity();
        userEntity.setId(owner.getUserId());
        return userEntity;
    }
}
