package com.trambambula.server.core.logging;

import com.trambambula.server.core.security.model.UserPrincipal;
import com.trambambula.server.core.util.SecurityUtils;
import com.trambambula.server.core.util.SessionUtils;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.UUID;

@Log4j2
public class Log4jContextEnhancerFilter extends OncePerRequestFilter {
    private static final String LOG_USER_ID_CONTEXT = "UID";
    private static final String LOG_SESSION_ID_CONTEXT = "SID";
    private static final String LOG_REQUEST_ID_CONTEXT = "RID";
    private static final int MAX_LENGTH = 8; // TODO remove


    @Override
    protected void doFilterInternal(final HttpServletRequest request,
                                    final HttpServletResponse response,
                                    final FilterChain filterChain) throws ServletException, IOException {
        try {
            assignLogContextValues(request);

            filterChain.doFilter(request, response);
        } finally {
            releaseContextValues();
        }
    }


    private void assignLogContextValues(final HttpServletRequest request) {
        ThreadContext.put(LOG_REQUEST_ID_CONTEXT, substring(UUID.randomUUID().toString()));
        SessionUtils.getSession(request)
            .map(HttpSession::getId)
            .map(this::substring)
            .ifPresent(sessionId -> ThreadContext.put(LOG_SESSION_ID_CONTEXT, sessionId));
        SecurityUtils.getPrincipal()
            .map(UserPrincipal::getId)
            .map(String::valueOf)
            .ifPresent(userId -> ThreadContext.put(LOG_USER_ID_CONTEXT, userId));
    }

    private void releaseContextValues() {
        ThreadContext.remove(LOG_REQUEST_ID_CONTEXT);
        ThreadContext.remove(LOG_SESSION_ID_CONTEXT);
        ThreadContext.remove(LOG_USER_ID_CONTEXT);
    }

    private String substring(final String value) {
        return StringUtils.isEmpty(value) || value.length() <= MAX_LENGTH
                ? value
                : value.substring(0, MAX_LENGTH);
    }
}
