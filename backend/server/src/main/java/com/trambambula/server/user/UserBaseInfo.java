package com.trambambula.server.user;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserBaseInfo {
    private long id;
    private String firstName;
    private String lastName;
    private String defaultDisplayName;
}
