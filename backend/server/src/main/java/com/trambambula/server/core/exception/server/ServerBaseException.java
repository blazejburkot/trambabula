package com.trambambula.server.core.exception.server;

import com.trambambula.server.core.exception.HttpBaseException;

/**
 * 5xx Server errors
 * Base class for exceptions thrown by this web-application
 * whenever the server encountered an error or is otherwise incapable of performing the request
 */
public abstract class ServerBaseException extends HttpBaseException {

    ServerBaseException(final String message) {
        super(message);
    }
}
