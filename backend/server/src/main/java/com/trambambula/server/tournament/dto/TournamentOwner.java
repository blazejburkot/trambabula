package com.trambambula.server.tournament.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TournamentOwner {
    private Long userId;
    private String firstName;
    private String lastName;
}
