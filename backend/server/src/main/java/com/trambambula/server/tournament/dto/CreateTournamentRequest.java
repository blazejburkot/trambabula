package com.trambambula.server.tournament.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.trambambula.db.model.tournament.TournamentType;
import com.trambambula.db.model.tournament.TournamentVisibility;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CreateTournamentRequest {

    @NotBlank
    @ApiParam(required = true)
    private String name;

    private String url;
    private LocalDateTime startDate;

    @ApiParam(required = true)
    @NotNull
    private TournamentType type;

    @ApiParam(required = true)
    @NotNull
    private TournamentSystem system;

    @ApiParam(required = true)
    @NotNull
    private TournamentVisibility visibility;

    @ApiParam(required = true)
    @NotNull
    private List<TournamentParticipant> participants;
}
