package com.trambambula.server.tournament.dto;

import com.trambambula.db.model.tournament.TournamentType;
import com.trambambula.db.model.tournament.system.TournamentSystemEntity;
import com.trambambula.db.model.tournament.system.TrambambulaSystemEntity;
import lombok.Getter;

import java.util.List;

@Getter
public enum TournamentSystem {
    TRAMBAMBULA(4, TrambambulaSystemEntity.class, TournamentType.INDIVIDUAL);

    private final int minimalNumberOfParticipants;
    private final Class<? extends TournamentSystemEntity> systemClass;
    private final List<TournamentType> enabledForTypes;

    TournamentSystem(final int minimalNumberOfParticipants,
                     final Class<? extends TournamentSystemEntity> systemClass,
                     final TournamentType... enabledForTypes) {
        this.minimalNumberOfParticipants = minimalNumberOfParticipants;
        this.systemClass = systemClass;
        this.enabledForTypes = List.of(enabledForTypes);
    }


    public static TournamentSystem fromTournamentSystemClass(final Class<? extends TournamentSystemEntity> systemClass) {
        for (TournamentSystem system : values()) {
            if (system.systemClass == systemClass) {
                return system;
            }
        }
        throw new IllegalArgumentException("Unknown system class: " + systemClass);
    }
}
