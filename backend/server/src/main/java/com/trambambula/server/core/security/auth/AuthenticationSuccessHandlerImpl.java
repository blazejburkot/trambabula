package com.trambambula.server.core.security.auth;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@SuppressWarnings("checkstyle:linelength")
public class AuthenticationSuccessHandlerImpl implements AuthenticationSuccessHandler {

    @Override
    public void onAuthenticationSuccess(
            final HttpServletRequest request, final HttpServletResponse response, final Authentication authentication) {
        clearAuthenticationAttributes(request);
    }

    /**
     * This method was copied from class {@link org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler#clearAuthenticationAttributes(HttpServletRequest) SimpleUrlAuthenticationSuccessHandler}
     */
    private void clearAuthenticationAttributes(final HttpServletRequest request) {
        final HttpSession session = request.getSession(false);

        if (session != null) {
            session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
        }
    }
}
