package com.trambambula.server.core.exception.client;

import org.springframework.http.HttpStatus;

@SuppressWarnings("squid:MaximumInheritanceDepth") // suppress sonar issue: Inheritance tree of classes should not be too deep
public class NotFoundException extends ClientBaseException {

    public NotFoundException(final String message) {
        super(message);
    }

    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.NOT_FOUND;
    }
}
