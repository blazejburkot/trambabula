package com.trambambula.server.tournament.dto;

import io.swagger.annotations.ApiParam;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotEmpty;

@Data
@EqualsAndHashCode(callSuper = true)
public class TournamentParticipant extends TournamentOwner {
    private long participantId;

    @ApiParam(required = true)
    @NotEmpty
    private String displayName;
}
