package com.trambambula.server.tournament.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.trambambula.db.model.tournament.TournamentStatus;
import com.trambambula.db.model.tournament.TournamentType;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TournamentOverviewData {
    private long id;
    private String name;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private TournamentType type;
    private TournamentStatus status;
    private TournamentSystem system;
}
