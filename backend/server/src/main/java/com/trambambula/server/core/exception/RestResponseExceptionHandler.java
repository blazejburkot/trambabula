package com.trambambula.server.core.exception;

import com.trambambula.server.core.exception.client.ClientBaseException;
import com.trambambula.server.core.exception.client.ForbiddenException;
import com.trambambula.server.core.exception.server.ServerBaseException;
import lombok.extern.log4j.Log4j2;
import org.apache.catalina.connector.ClientAbortException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@Log4j2
@ControllerAdvice
@ResponseBody
public class RestResponseExceptionHandler {

    @ExceptionHandler({ClientAbortException.class, MissingServletRequestParameterException.class, BindException.class,
            MethodArgumentNotValidException.class})
    public ResponseEntity<ExceptionResponse> handleBadRequestTypeException(final HttpServletRequest request, final Exception ex) {
        log.info("{}: '{}' request: {}", getType(ex), ex.getMessage(), request.getRequestURL());
        return transform(ex, request, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<ExceptionResponse> handleAccessDeniedException(final HttpServletRequest request,
                                                                         final AccessDeniedException ex) {
        return handleClientException(request, new ForbiddenException(ex.getMessage(), ex));
    }

    @ExceptionHandler(ClientBaseException.class)
    public ResponseEntity<ExceptionResponse> handleClientException(final HttpServletRequest request, final ClientBaseException ex) {
        log.info("{}: '{}' request: {}", getType(ex), ex.getMessage(), request.getRequestURL());
        return transform(ex, request, ex.getHttpStatus());
    }

    @ExceptionHandler(ServerBaseException.class)
    public ResponseEntity<ExceptionResponse> handleServerException(final HttpServletRequest request, final ServerBaseException ex) {
        log.error("{}: '{}' request: {}", getType(ex), ex.getMessage(), request.getRequestURL(), ex);
        return transform(ex, request, ex.getHttpStatus());
    }

    @ExceptionHandler(Throwable.class)
    public ResponseEntity<ExceptionResponse> handleException(final HttpServletRequest request, final Throwable ex) {
        log.error("Unhandled exception {}: '{}' request: {}", getType(ex), ex.getMessage(), request.getRequestURL(), ex);
        return transform(ex, request, HttpStatus.INTERNAL_SERVER_ERROR);
    }


    private ResponseEntity<ExceptionResponse> transform(final Throwable exception,
                                                        final HttpServletRequest request,
                                                        final HttpStatus httpStatus) {
        ExceptionResponse response = new ExceptionResponse(exception, request.getMethod(), request.getRequestURL().toString(), httpStatus);
        return ResponseEntity
                .status(httpStatus)
                .body(response);
    }

    private String getType(final Throwable ex) {
        return ex.getClass().getSimpleName();
    }
}
