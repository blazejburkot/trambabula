package com.trambambula.server.core.security.auth;

import com.trambambula.db.model.user.UserRole;

public final class Authorities {
    public static final String ADMIN = UserRole.ADMIN.name();

    public static final String IS_ADMIN = "hasAuthority('ADMIN')";

    private Authorities() {
    }
}
