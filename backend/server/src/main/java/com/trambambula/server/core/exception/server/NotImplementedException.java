package com.trambambula.server.core.exception.server;

import org.springframework.http.HttpStatus;

@SuppressWarnings("squid:MaximumInheritanceDepth") // suppress sonar issue: Inheritance tree of classes should not be too deep
public class NotImplementedException extends ServerBaseException {

    public NotImplementedException(final String message) {
        super(message);
    }

    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.NOT_IMPLEMENTED;
    }
}
