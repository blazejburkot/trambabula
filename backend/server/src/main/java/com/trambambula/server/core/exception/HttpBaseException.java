package com.trambambula.server.core.exception;

import org.springframework.http.HttpStatus;

public abstract class HttpBaseException extends RuntimeException {

    public HttpBaseException(final String message) {
        super(message);
    }

    public HttpBaseException(final String message, final Exception ex) {
        super(message, ex);
    }

    public HttpBaseException(final Exception ex) {
        super(ex);
    }

    public abstract HttpStatus getHttpStatus();
}
