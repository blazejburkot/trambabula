package com.trambambula.server.user;

import com.trambambula.db.model.user.UserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.factory.Mappers;

@Mapper(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
interface UserMapper {
    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    @Mapping(target = "version", ignore = true)
    @Mapping(target = "defaultDisplayName", ignore = true)
    @Mapping(target = "lastModificationDate", ignore = true)
    UserDetails mapBaseFields(UserEntity userEntity);

    UserDetails mapAllFields(UserEntity userEntity);

    UserBaseInfo mapBaseInfo(UserEntity entity);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "password", ignore = true)
    @Mapping(target = "lastModificationDate", ignore = true)
    void applyNotNullChanges(@MappingTarget UserEntity userEntity, UserDetails changes);


    @Mapping(target = "id", ignore = true)
    @Mapping(target = "password", ignore = true)
    @Mapping(target = "version", ignore = true)
    @Mapping(target = "defaultDisplayName", ignore = true)
    @Mapping(target = "role", ignore = true)
    @Mapping(target = "lastModificationDate", ignore = true)
    UserEntity mapUser(RegistrationForm registrationForm);
}
