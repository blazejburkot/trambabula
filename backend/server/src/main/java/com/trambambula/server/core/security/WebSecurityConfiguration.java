package com.trambambula.server.core.security;

import com.trambambula.server.core.logging.Log4jContextEnhancerFilter;
import com.trambambula.server.core.security.auth.Authorities;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.header.HeaderWriterFilter;

@Log4j2
@Configuration
@EnableWebSecurity//(debug = true)
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Import(SecurityComponents.class)
@RequiredArgsConstructor
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {
    private static final String[] GENERAL_ENDPOINTS = {"/api/v1/login", "/error"}; // TODO Issue #9, remove /error
    private static final String HEALTH_ENDPOINT = "/actuator/health";
    private static final String[] ACTUATOR_ENDPOINTS = {"/actuator", "/actuator/**"};
    private static final String[] SWAGGER_ENDPOINTS = {"/swagger-resources/**", "/swagger-ui.html", "/v2/api-docs", "/webjars/**"};
    private static final String[] PUBLIC_TOURNAMENTS_ENDPOINTS = {"/api/v1/tournaments", "/api/v1/tournaments/*"};
    private static final String CREATE_USER_ENDPOINT = "/api/v1/users";
    private static final String[] H2_ENDPOINTS = {"/h2", "/h2/**"};

    private final ServerProperties serverProperties;
    private final SecurityComponents securityComponents;

    @Override
    public void configure(final AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder
                .userDetailsService(securityComponents.userDetailsService())
                .passwordEncoder(securityComponents.passwordEncoder());
    }

    @Override
    public void configure(final HttpSecurity httpSecurity) throws Exception {
        httpSecurity
                .addFilterAfter(securityComponents.log4jContextEnhancerFilter(), HeaderWriterFilter.class)
                .addFilterAfter(securityComponents.requestLoggingFilter(), Log4jContextEnhancerFilter.class);

        httpSecurity.csrf()
                .disable();

        httpSecurity.authorizeRequests()
                .antMatchers(HttpMethod.OPTIONS).permitAll()
                .antMatchers(HttpMethod.POST, CREATE_USER_ENDPOINT).permitAll()
                .antMatchers(HttpMethod.GET, GENERAL_ENDPOINTS).permitAll()
                .antMatchers(HttpMethod.GET, PUBLIC_TOURNAMENTS_ENDPOINTS).permitAll()
                .antMatchers(HttpMethod.GET, HEALTH_ENDPOINT).permitAll()

                .antMatchers(HttpMethod.GET, SWAGGER_ENDPOINTS).authenticated()

                .antMatchers(ACTUATOR_ENDPOINTS).hasAuthority(Authorities.ADMIN)
                .antMatchers(H2_ENDPOINTS).hasAuthority(Authorities.ADMIN)

                .anyRequest().authenticated(); // TODO Issue #9, change to denyAll()

        httpSecurity.headers()
                .frameOptions().sameOrigin();

        httpSecurity.exceptionHandling()
                .authenticationEntryPoint(securityComponents.authenticationEntryPoint());


        httpSecurity.httpBasic();

        httpSecurity.formLogin() // TODO Issue #9
                .loginProcessingUrl("/api/v1/login")
                .successHandler(securityComponents.authenticationSuccessHandler())
                .failureHandler(securityComponents.authenticationFailureHandler());

        httpSecurity.logout()
                .logoutUrl("/api/v1/logout")
                .logoutSuccessHandler(securityComponents.logoutSuccessHandler())
                .invalidateHttpSession(true)
                .deleteCookies(serverProperties.getServlet().getSession().getCookie().getName());
    }
}
