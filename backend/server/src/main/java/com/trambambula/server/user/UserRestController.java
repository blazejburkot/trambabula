package com.trambambula.server.user;

import com.trambambula.server.core.exception.client.BadRequestException;
import com.trambambula.server.core.security.auth.Authorities;
import com.trambambula.server.core.security.model.UserPrincipal;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Log4j2
@RestController
@RequestMapping(value = "/api/v1/users")
@RequiredArgsConstructor
class UserRestController {
    private static final String USER_ENDPOINT = "/{id:\\d+}";
    private static final String USER_PASSWORD_ENDPOINT = "/{id:\\d+}/password";
    private static final String IS_ADMIN_OR_IS_DATA_OWNER = Authorities.IS_ADMIN + " || authentication.principal.id == #id";
    private static final int LIMIT_SEARCH_BY_USER_NAME = 3;

    private final UserService service;

    @GetMapping(params = "query")
    @PreAuthorize(Authorities.IS_ADMIN)
    Page<UserDetails> search(@RequestParam(value = "query", defaultValue = "") final String query, final Pageable pageable) {
        return service.search(query, pageable);
    }

    @GetMapping(params = "userName")
    Page<UserBaseInfo> searchByUserName(@RequestParam(value = "userName") final String query) {
        return service.searchByUserName(query, PageRequest.of(0, LIMIT_SEARCH_BY_USER_NAME));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    void createUser(@Valid @RequestBody final RegistrationForm registrationForm) {
        if (registrationForm.getFirstName() == null) {
            registrationForm.setFirstName("");
        }
        if (registrationForm.getLastName() == null) {
            registrationForm.setLastName("");
        }

        service.creteUser(registrationForm);
    }

    @GetMapping("/me")
    UserDetails getPrincipal(final Authentication authentication) {
        final UserPrincipal principal = (UserPrincipal) authentication.getPrincipal();
        return service.getById(principal.getId());
    }

    @GetMapping(USER_ENDPOINT)
    @PreAuthorize(IS_ADMIN_OR_IS_DATA_OWNER)
    UserDetails getUser(@PathVariable final long id) {
        return service.getById(id);
    }

    @PatchMapping(USER_ENDPOINT)
    @PreAuthorize(IS_ADMIN_OR_IS_DATA_OWNER)
    void patchUser(@PathVariable final long id, @RequestBody final UserDetails userUpdates) {
        if (userUpdates == null) {
            return;
        }

        service.updateUser(id, userUpdates);
    }

    @PutMapping(USER_PASSWORD_ENDPOINT)
    @PreAuthorize(IS_ADMIN_OR_IS_DATA_OWNER)
    void changePassword(@PathVariable final long id, @RequestBody final String newPassword) {
        final String newPasswordTrimmed = newPassword.trim();
        if (StringUtils.isEmpty(newPasswordTrimmed)) {
            throw new BadRequestException("password cannot be empty");
        }

        service.changePassword(id, newPasswordTrimmed);
    }
}
