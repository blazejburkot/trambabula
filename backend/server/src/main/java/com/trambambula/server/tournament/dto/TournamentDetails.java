package com.trambambula.server.tournament.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.trambambula.db.model.tournament.TournamentVisibility;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TournamentDetails extends TournamentOverviewData {
    private Long version;
    private LocalDateTime lastModificationDate;
    private String url;
    private TournamentVisibility visibility;
    private TournamentOwner owner;
    private List<TournamentParticipant> participants;
    private List<TournamentStage> stages;

    @Override
    public String toString() {
        return "TournamentDetails{"
                + "id=" + getId()
                + ", version=" + version
                + ", name='" + getName() + '\''
                + '}';
    }
}
