package com.trambambula.server.core.logging;

import lombok.Getter;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.WebUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * <p>This is copy of {@link org.springframework.web.filter.AbstractRequestLoggingFilter}</p>
 * <p>It was needed because we want to log response code</p>
 */
@Getter
public class RequestLoggingFilter extends OncePerRequestFilter {
    private final boolean includeQueryString = true;
    private final boolean includeClientInfo = true;
    private final boolean includeHeaders = true;
    private final boolean includePayload = true;
    private final int maxPayloadLength = 50;


    /**
     * @see org.springframework.web.filter.AbstractRequestLoggingFilter#shouldNotFilterAsyncDispatch()
     */
    @Override
    protected boolean shouldNotFilterAsyncDispatch() {
        return false;
    }

    @Override
    protected void doFilterInternal(final HttpServletRequest request,
                                    final HttpServletResponse response,
                                    final FilterChain filterChain) throws ServletException, IOException {
        final boolean isFirstRequest = !isAsyncDispatch(request);
        HttpServletRequest requestToUse = request;

        if (isIncludePayload() && isFirstRequest && !(request instanceof ContentCachingRequestWrapper)) {
            requestToUse = new ContentCachingRequestWrapper(request, getMaxPayloadLength());
        }

        boolean shouldLog = logger.isDebugEnabled();
        if (shouldLog && isFirstRequest) {
            this.logger.debug(getBeforeMessage(requestToUse));
        }

        try {
            filterChain.doFilter(requestToUse, response);
        } finally {
            if (shouldLog && !isAsyncStarted(requestToUse)) {
                this.logger.debug(getAfterMessage(request, response));
            }
        }
    }


    protected String getBeforeMessage(final HttpServletRequest request) {
        final StringBuilder msg = new StringBuilder();
        msg.append("Before request [method=");
        msg.append(request.getMethod());
        msg.append(";uri=").append(request.getRequestURI());

        if (isIncludeQueryString()) {
            String queryString = request.getQueryString();
            if (queryString != null) {
                msg.append('?').append(queryString);
            }
        }

        if (isIncludeClientInfo()) {
            String client = request.getRemoteAddr();
            if (StringUtils.hasLength(client)) {
                msg.append(";client=").append(client);
            }
            HttpSession session = request.getSession(false);
            if (session != null) {
                msg.append(";session=").append(session.getId());
            }
            String user = request.getRemoteUser();
            if (user != null) {
                msg.append(";user=").append(user);
            }
        }

        if (isIncludeHeaders()) {
            msg.append(";headers=").append(new ServletServerHttpRequest(request).getHeaders());
        }

        if (isIncludePayload()) {
            String payload = getMessagePayload(request);
            if (payload != null) {
                msg.append(";payload=").append(payload);
            }
        }

        msg.append(']');
        return msg.toString();
    }

    protected String getAfterMessage(final HttpServletRequest request, final HttpServletResponse response) {
        final StringBuilder msg = new StringBuilder();
        msg.append("After request [method=");
        msg.append(request.getMethod());
        msg.append(";uri=").append(request.getRequestURI());

        msg.append("] Response [");
        msg.append("status=").append(response.getStatus());

        msg.append(']');
        return msg.toString();
    }

    protected String getMessagePayload(final HttpServletRequest request) {
        final ContentCachingRequestWrapper wrapper = WebUtils.getNativeRequest(request, ContentCachingRequestWrapper.class);
        if (wrapper != null) {
            byte[] buf = wrapper.getContentAsByteArray();
            if (buf.length > 0) {
                int length = Math.min(buf.length, getMaxPayloadLength());
                try {
                    return new String(buf, 0, length, wrapper.getCharacterEncoding());
                } catch (UnsupportedEncodingException ex) {
                    return "[unknown]";
                }
            }
        }
        return null;
    }
}
