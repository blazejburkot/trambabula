package com.trambambula.server.core.security.auth;

import com.trambambula.db.dao.UserRepository;
import com.trambambula.db.model.user.UserEntity;
import com.trambambula.server.core.security.model.UserPrincipal;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.util.StringUtils;

import java.util.List;

@Log4j2
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {
    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(final String login) throws UsernameNotFoundException {
        log.debug("login: {}", login);
        if (StringUtils.isEmpty(login)) {
            throw new UsernameNotFoundException(login);
        }

        return userRepository.findUserByEmailIgnoreCase(login)
                .map(this::mapToUserDetails)
                .orElseThrow(() -> new UsernameNotFoundException(login));
    }

    private UserDetails mapToUserDetails(final UserEntity userEntity) {
        final List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList(userEntity.getRole().toString());
        return new UserPrincipal(userEntity.getId(), userEntity.getEmail(), userEntity.getPassword(), authorities);
    }
}
