package com.trambambula.server.user;

import com.google.common.collect.Lists;
import com.trambambula.db.dao.UserRepository;
import com.trambambula.db.model.user.UserEntity;
import com.trambambula.db.model.user.UserRole;
import com.trambambula.server.core.exception.client.ConflictException;
import com.trambambula.server.core.exception.client.NotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.persistence.OptimisticLockException;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;

@Log4j2
@Service
@RequiredArgsConstructor
class UserService {
    private final UserMapper mapper = UserMapper.INSTANCE;
    private final UserRepository repository;
    private final PasswordEncoder passwordEncoder;

    @Transactional(readOnly = true)
    UserDetails getById(final long id) {
        final UserEntity userEntity = getUserEntity(id);
        return mapper.mapAllFields(userEntity);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    void updateUser(final long id, final UserDetails changes) {
        final UserEntity userEntity = getUserEntity(id);

        if (!Objects.equals(userEntity.getVersion(), changes.getVersion())) {
            throw new ConflictException(new OptimisticLockException("User{id: " + id + "} was updated by another thread"));
        }

        final String newUserEmail = changes.getEmail();
        if (newUserEmail != null && !userEntity.getEmail().equalsIgnoreCase(newUserEmail)) {
            checkIfEmailIsNotUsed(newUserEmail);
        }

        mapper.applyNotNullChanges(userEntity, changes);

        repository.save(userEntity);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    void changePassword(final long id, final String newPassword) {
        final UserEntity user = getUserEntity(id);
        setEncodedPassword(user, newPassword);

        repository.save(user);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    void creteUser(final RegistrationForm registrationForm) {
        final UserEntity user = mapper.mapUser(registrationForm);

        checkIfEmailIsNotUsed(user.getEmail());
        setEncodedPassword(user, registrationForm.getPassword());
        setDefaultDisplayName(user);

        final UserRole userRole = repository.count() == 0 ? UserRole.ADMIN : UserRole.USER;
        user.setRole(userRole);

        final UserEntity userEntity = repository.save(user);
        log.debug("user {}", userEntity);
    }

    @Transactional(readOnly = true)
    Page<UserDetails> search(final String query, final Pageable pageable) {
        final Page<UserEntity> entities = StringUtils.isEmpty(query)
                ? repository.findAll(pageable)
                : repository.search(matchSqlQuery(query), pageable);
        final List<UserDetails> users = Lists.transform(entities.getContent(), mapper::mapBaseFields);
        return new PageImpl<>(users, pageable, entities.getTotalElements());
    }

    @Transactional(readOnly = true)
    Page<UserBaseInfo> searchByUserName(final String query, final Pageable pageable) {
        final Page<UserEntity> entities = repository.searchByUserName(matchSqlQuery(query), pageable);
        final List<UserBaseInfo> users = Lists.transform(entities.getContent(), mapper::mapBaseInfo);
        return new PageImpl<>(users, pageable, entities.getTotalElements());
    }


    private void checkIfEmailIsNotUsed(final String userEmail) {
        if (repository.findUserByEmailIgnoreCase(userEmail).isPresent()) {
            throw new ConflictException("User already exists: " + userEmail);
        }
    }

    private void setEncodedPassword(final UserEntity user, final String rawPassword) {
        final String passwordEncoded = passwordEncoder.encode(rawPassword);
        user.setPassword(passwordEncoded);
    }

    private UserEntity getUserEntity(final long id) {
        return repository.findById(id)
            .orElseThrow(() -> new NotFoundException("User not found: " + id));
    }

    private String matchSqlQuery(final String query) {
        return "%" + query.replaceAll("\\s+", "%").toLowerCase() + "%";
    }

    private void setDefaultDisplayName(final UserEntity user) {
        final Function<String, String> getFirstChar = value -> StringUtils.isEmpty(value) ? "" : value.substring(0, 1).toUpperCase();
        final String defaultDisplayName =  getFirstChar.apply(user.getFirstName()) + getFirstChar.apply(user.getLastName());
        user.setDefaultDisplayName(defaultDisplayName);
    }
}
