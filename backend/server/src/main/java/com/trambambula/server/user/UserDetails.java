package com.trambambula.server.user;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.trambambula.db.model.user.UserRole;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
class UserDetails {
    private long id;
    private Long version;
    private LocalDateTime lastModificationDate;
    private String email;
    private String firstName;
    private String lastName;
    private String defaultDisplayName;
    private UserRole role;
}
