package com.trambambula.server.tournament.dto;

import lombok.Data;

@Data
public class TournamentSearchRequest {
    private String name;
}
