package com.trambambula.server.tournament;

import org.springframework.stereotype.Component;

import java.security.SecureRandom;
import java.util.UUID;

@Component
class RandomGenerator {
    private final SecureRandom random = new SecureRandom();

    String randomUrl() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    boolean randomBoolean() {
        return random.nextBoolean();
    }
}
