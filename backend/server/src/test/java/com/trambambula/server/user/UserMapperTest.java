package com.trambambula.server.user;

import com.trambambula.db.model.user.UserEntity;
import com.trambambula.db.model.user.UserRole;
import org.junit.Test;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;

public class UserMapperTest {
    private static final int USER_ID = 123;

    private UserMapper mapper = UserMapper.INSTANCE;

    @Test
    public void shouldApplyOnlyNotNullChanges() {
        // given
        final UserEntity userEntity = createUser();
        final UserDetails changes = new UserDetails();
        final String newFirstName = "User";
        changes.setId(USER_ID + 100);
        changes.setFirstName(newFirstName);

        // when
        mapper.applyNotNullChanges(userEntity, changes);

        // then
        assertThat(userEntity).hasNoNullFieldsOrProperties();
        assertThat(userEntity.getId()).isEqualTo(USER_ID);
        assertThat(userEntity.getFirstName()).isEqualTo(newFirstName);
    }

    private UserEntity createUser() {
        final UserEntity user = new UserEntity();
        user.setId(USER_ID);
        user.setVersion(5L);
        user.setLastModificationDate(LocalDateTime.of(2000, 2, 2, 2, 2, 2, 2));
        user.setEmail("user@example.com");
        user.setPassword("{noop}password");
        user.setFirstName("John");
        user.setLastName("Smith");
        user.setDefaultDisplayName("JS");
        user.setRole(UserRole.USER);
        return user;
    }
}
