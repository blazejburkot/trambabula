package com.trambambula.server.tournament;

import com.trambambula.db.model.tournament.TournamentParticipantEntity;
import com.trambambula.db.model.tournament.system.TrambambulaSystemEntity;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class TournamentServiceTest {

    @Test
    public void shouldCreateTrambambulaTournament() {
        // given
        final TournamentService service = new TournamentService(new RandomGenerator(), null, null, null);
        final Collection<TournamentParticipantEntity> participants = createParticipants("0", "1", "2", "3", "4", "5");

        // when
        final TrambambulaSystemEntity tournamentSystem = service.createTrambambulaTournament(participants);

        // then
        assertThat(tournamentSystem.getStage().getMatches()).hasSize(45);
    }

    private Collection<TournamentParticipantEntity> createParticipants(final String... names) {
        final List<TournamentParticipantEntity> participants = new ArrayList<>();
        for (String name : names) {
            final TournamentParticipantEntity entity = new TournamentParticipantEntity();
            entity.setDisplayName(name);
            participants.add(entity);
        }

        return participants;
    }
}
