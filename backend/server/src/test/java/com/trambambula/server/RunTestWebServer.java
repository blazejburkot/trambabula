package com.trambambula.server;

import org.springframework.boot.SpringApplication;

public class RunTestWebServer {

    public static void main(final String[] args) {
        System.setProperty("spring.profiles.active", "integration-test");
        SpringApplication.run(TrambambulaWebServer.class, args);
    }
}
