package com.trambambula.e2e;

import com.trambambula.server.TrambambulaWebServer;
import com.trambambula.server.core.security.model.UserPrincipal;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.equalTo;
import static org.springframework.security.core.authority.AuthorityUtils.createAuthorityList;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest(classes = TrambambulaWebServer.class)
@ActiveProfiles("integration-test")
public class TrambambulaWebServerE2ETest {
    protected static final String ADMIN_USER_NAME = "mica@example.com";
    protected static final String ADMIN_PASSWORD = "password";
    private static final UserPrincipal ADMIN = new UserPrincipal(5, ADMIN_USER_NAME, ADMIN_PASSWORD, createAuthorityList("ADMIN"));
    private static final UserPrincipal USER = new UserPrincipal(6, "paige@example.com", "password", createAuthorityList("USER"));

    @Autowired
    private MockMvc mockMvc;


    @Test
    public void shouldReturnUnauthorizedWhenThereIsNoPrincipal() throws Exception {
        mockMvc.perform(get("/api/v1/users/2"))
            .andExpect(status().isUnauthorized());
    }

    @Test
    public void shouldReturnUnauthorizedWhenThePrincipalIsNotAdminAndThisIsNotHisData() throws Exception {
        mockMvc.perform(
            get("/api/v1/users/2")
            .with(user(USER))
        )
            .andExpect(status().isForbidden());
    }

    @Test
    public void shouldReturnDataWhenThePrincipalIsDataOwner() throws Exception {
        mockMvc.perform(
            get("/api/v1/users/6")
            .with(user(USER))
        )
            .andExpect(status().isOk())
            .andExpect(jsonPath("email", equalTo("paige@example.com")))
        ;
    }

    @Test
    public void shouldReturnDataWhenThePrincipalIsAdmin() throws Exception {
        mockMvc.perform(
            get("/api/v1/users/2")
            .with(user(ADMIN))
        )
            .andExpect(status().isOk())
            .andExpect(jsonPath("email", equalTo("shara@example.com")))
        ;
    }

    @Test
    public void shouldAuthenticateUser() throws Exception {
        mockMvc.perform(
            post("/api/v1/login")
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .param("username", ADMIN_USER_NAME)
                .param("password", ADMIN_PASSWORD)
        )
            .andExpect(status().isOk())
            .andExpect(authenticated().withUsername(ADMIN_USER_NAME))
            .andExpect(content().string(""));
    }

    @Test
    public void shouldReturnAllPublicTournaments() throws Exception {
        mockMvc.perform(get("/api/v1/tournaments"))
            .andExpect(status().isOk());
    }
}
