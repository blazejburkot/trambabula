-- USERS DATA
insert into users_tbl (id, version, email, role, password, first_name, last_name, default_display_name)
values (1, 1, 'blazej@example.com', 'ADMIN', '{bcrypt}$2a$10$mXefPFUbAKf1/.urGiys3uopLsd964HRUTJNPy29JmFqUNcBltyJ6', 'Blazej', '', '');

insert into users_tbl (id, version, email, role, password, first_name, last_name, default_display_name)
values (2, 1, 'shara@example.com', 'USER', '{noop}password', 'Shara', 'Granger', 'SG');

insert into users_tbl (id, version, email, role, password, first_name, last_name, default_display_name)
values (3, 1, 'dana@example.com', 'USER', '{noop}password', 'Dana', 'Sumpter', 'D');

insert into users_tbl (id, version, email, role, password, first_name, last_name, default_display_name)
values (4, 1, 'donita@example.com', 'USER', '{noop}password', 'Donita', 'Biros', 'D');

insert into users_tbl (id, version, email, role, password, first_name, last_name, default_display_name)
values (5, 1, 'mica@example.com', 'ADMIN', '{noop}password', 'Mica', 'Raap', 'M');

insert into users_tbl (id, version, email, role, password, first_name, last_name, default_display_name)
values (6, 1, 'paige@example.com', 'USER', '{noop}password', 'Paige', 'Osburn', 'P');


-- TOURNAMENT id: 1, name: 'Tournament 1'
insert into tournaments_tbl (id, owner_id, version, name, url, start_date, type, visibility, tournament_system_id, status)
values (1, 1, 1, 'Tournament 1', 'bae6b23cf3a04719b1b158ddb6ab3766', '2012-09-17 18:47:52.69', 'INDIVIDUAL', 'PUBLIC', 1, 'STARTED');

insert into tournament_participants_tbl (id, version, display_name, tournament_id, user_id) values (1, 1, 'Blazej', 1, 1);
insert into tournament_participants_tbl (id, version, display_name, tournament_id, user_id) values (2, 1, 'SG', 1, 2);
insert into tournament_participants_tbl (id, version, display_name, tournament_id, user_id) values (3, 1, 'Jane', 1, null);
insert into tournament_participants_tbl (id, version, display_name, tournament_id, user_id) values (4, 1, 'John', 1, null);

insert into tournament_systems_tbl (id, type, name) values (1, 'TRAMBAMBULA', null);
insert into tournament_stages_tbl (id, tournament_system_id) values (1, 1);

insert into tournament_matches_tbl (id, tournament_stage_id, host_points, guest_points, date_time, description) values (1, 1, 1, -1, '2012-09-17 18:47:52.69', '2:1');
insert into tournament_match_guests_tbl (match_id, participant_id) values (1, 1);
insert into tournament_match_guests_tbl (match_id, participant_id) values (1, 2);
insert into tournament_match_hosts_tbl (match_id, participant_id) values (1, 3);
insert into tournament_match_hosts_tbl (match_id, participant_id) values (1, 4);

insert into tournament_matches_tbl (id, tournament_stage_id) values (2, 1);
insert into tournament_match_guests_tbl (match_id, participant_id) values (2, 1);
insert into tournament_match_guests_tbl (match_id, participant_id) values (2, 3);
insert into tournament_match_hosts_tbl (match_id, participant_id) values (2, 2);
insert into tournament_match_hosts_tbl (match_id, participant_id) values (2, 4);

insert into tournament_matches_tbl (id, tournament_stage_id) values (3, 1);
insert into tournament_match_guests_tbl (match_id, participant_id) values (3, 1);
insert into tournament_match_guests_tbl (match_id, participant_id) values (3, 4);
insert into tournament_match_hosts_tbl (match_id, participant_id) values (3, 2);
insert into tournament_match_hosts_tbl (match_id, participant_id) values (3, 3);
-- END DEFINITION OF TOURNAMENT id: 1
