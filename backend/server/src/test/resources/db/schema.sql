create table users_tbl
(
    id                     bigint identity,
    last_modification_date datetime      not null default CURRENT_DATE(),
    version                bigint        not null default 1,
    email                  nvarchar(128) not null,
    role                   nvarchar(10)  not null,
    password               nvarchar(256) not null,
    first_name             nvarchar(128) not null,
    default_display_name   nvarchar(128) not null,
    last_name              nvarchar(128) not null
);

create table tournaments_tbl
(
    id                     bigint identity,
    last_modification_date datetime      not null default CURRENT_DATE(),
    start_date             datetime      null,
    end_date               datetime      null,
    version                bigint        not null default 1,
    name                   nvarchar(128) not null,
    url                    nvarchar(128) null,
    type                   nvarchar(20)  not null,
    status                 nvarchar(20)  not null,
    visibility             nvarchar(20)  not null,
    owner_id               bigint        not null,
    tournament_system_id   bigint        not null,
    UNIQUE KEY ux_tournament_url (url)
);

create table tournament_participants_tbl
(
    id                     bigint identity,
    last_modification_date datetime      not null default CURRENT_DATE(),
    version                bigint        not null default 1,
    display_name           nvarchar(128) not null,
    tournament_id          bigint        not null,
    user_id                bigint        null,
    UNIQUE KEY ux_tournament_id__display_name (tournament_id, display_name)
);

create table tournament_systems_tbl
(
    id                     bigint identity,
    last_modification_date datetime      not null default CURRENT_DATE(),
    version                bigint        not null default 1,
    type                   nvarchar(20)  not null,
    name                   nvarchar(128) null
);

create table tournament_stages_tbl
(
    id                     bigint identity,
    last_modification_date datetime not null default CURRENT_DATE(),
    version                bigint   not null default 1,
    phrase                 integer  not null default 1,
    tournament_system_id   bigint   not null
);

create table tournament_matches_tbl
(
    id                     bigint identity,
    last_modification_date datetime      not null default CURRENT_DATE(),
    version                bigint        not null default 1,
    tournament_stage_id    bigint        not null,
    host_points            integer       null,
    guest_points           integer       null,
    date_time              datetime      null,
    description            nvarchar(128) null,
);

create table tournament_match_guests_tbl
(
    participant_id bigint not null,
    match_id       bigint not null
);

create table tournament_match_hosts_tbl
(
    participant_id bigint not null,
    match_id       bigint not null
);
