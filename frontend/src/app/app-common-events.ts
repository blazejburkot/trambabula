import {Action} from '@ngrx/store';
import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {MatSnackBar} from '@angular/material/snack-bar';
import {tap} from 'rxjs/operators';
import {TranslateService} from '@ngx-translate/core';
import {LoggerService} from './shared/logger/logger.service';

export enum AppCommonActionTypes {
    ActionFail = '[App] Action fail'
}

export class ActionFail implements Action {
    readonly type = AppCommonActionTypes.ActionFail;

    constructor(public readonly actionType: string,
                public readonly error: any) {
    }
}

@Injectable({providedIn: 'root'})
export class AppCommonEffects {

    @Effect({dispatch: false})
    showErrorDialog = this.actions
        .pipe(
            ofType<ActionFail>(AppCommonActionTypes.ActionFail),
            tap(actionFail => {
                this.logger.error(actionFail);
                const msg = this.translationService.instant('error.serverError');
                this.snackBarErrorMsg.open(msg, '', {duration: 3000});
            })
        );


    constructor(private actions: Actions,
                private logger: LoggerService,
                private translationService: TranslateService,
                private snackBarErrorMsg: MatSnackBar) {
    }
}
