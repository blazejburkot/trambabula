import {Component} from '@angular/core';
import {TranslatePipe} from '@ngx-translate/core';
import {Store} from '@ngrx/store';
import {AppState} from './app.state';
import {LanguageSetInitialAction} from './shared/language/language.actions';
import {AuthTryLoadPrincipal} from './shared/auth/auth.actions';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    providers: [TranslatePipe]
})
export class AppComponent {
    constructor(private store: Store<AppState>) {

        // SET LANGUAGE
        this.store.dispatch(new LanguageSetInitialAction());

        // LOAD PRINCIPAL
        this.store.dispatch(new AuthTryLoadPrincipal());
    }
}
