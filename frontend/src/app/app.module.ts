import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {TournamentsModule} from './feature/tournaments/tournaments.module';
import {LoginModule} from './feature/login/login.module';
import {RegisterModule} from './feature/register/register.module';
import {ProfileModule} from './feature/profile/profile.module';
import {UsersModule} from './feature/users/users.module';
import {MatSidenavModule} from '@angular/material/sidenav';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {MatIconModule} from '@angular/material/icon';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatListModule} from '@angular/material/list';
import {LoggerService} from './shared/logger/logger.service';
import {ConsoleLoggerService} from './shared/logger/console-logger.service';
import {GlobalErrorHandler} from './shared/handler/global-error-handler';
import {HttpErrorInterceptor} from './shared/interceptor/http-error-interceptor';
import {NavbarModule} from './navbar/navbar.module';
import {BrowserCookiesModule, BrowserCookiesService} from '@ngx-utils/cookies/browser';
import {StoreModule} from '@ngrx/store';
import {appStateReducers} from './app.state';
import {EffectsModule} from '@ngrx/effects';
import {LanguageEffects} from './shared/language/language.effects';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {environment} from '../environments/environment';
import {AuthEffects} from './shared/auth/auth.effects';
import {FlexLayoutModule} from '@angular/flex-layout';
import {RegisterEffects} from './feature/register/register.store';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {AppCommonEffects} from './app-common-events';

export function HttpLoaderFactory(httpClient: HttpClient) {
    return new TranslateHttpLoader(httpClient);
}

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        AppRoutingModule,
        NavbarModule,
        TournamentsModule,
        LoginModule,
        RegisterModule,
        ProfileModule,
        UsersModule,

        HttpClientModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }),

        BrowserModule,
        BrowserAnimationsModule,
        BrowserCookiesModule.forRoot(),
        MatIconModule,
        MatSnackBarModule,
        MatSidenavModule,
        MatToolbarModule,
        MatListModule,
        FlexLayoutModule,

        StoreModule.forRoot(appStateReducers),
        EffectsModule.forRoot([LanguageEffects, AuthEffects, RegisterEffects, AppCommonEffects]),
        StoreDevtoolsModule.instrument({maxAge: 25, logOnly: environment.production})
    ],
    providers: [BrowserCookiesService,
        {provide: LoggerService, useClass: ConsoleLoggerService},
        {provide: ErrorHandler, useClass: GlobalErrorHandler},
        {provide: HTTP_INTERCEPTORS, useClass: HttpErrorInterceptor, multi: true}
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
