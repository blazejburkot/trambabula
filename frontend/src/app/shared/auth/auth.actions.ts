import {Action} from '@ngrx/store';
import {User} from '../model/users.model';

export enum AuthActionTypes {
    TryLoadPrincipal = '[Auth] Try Load Principal',
    LoadedPrincipal = '[Auth] Loaded Principal',
    LoadingPrincipalFailed = '[Auth] Loading Principal Failed',

    LoginAttempt = '[Auth] Login Attempt',
    LoginSucceed = '[Auth] Login Succeed',
    LoginFail = '[Auth] Login Fail',

    Logout = '[Auth] Logout'
}

export abstract class AuthAction implements Action {
    readonly type;
}

export class AuthTryLoadPrincipal {
    readonly type = AuthActionTypes.TryLoadPrincipal;
}

export class AuthPrincipalLoaded extends AuthAction {
    readonly type = AuthActionTypes.LoadedPrincipal;

    constructor(public readonly user: User) {
        super();
    }
}

export class AuthLoadingPrincipalFailed extends AuthAction {
    readonly type = AuthActionTypes.LoadingPrincipalFailed;

    constructor(public readonly error: any) {
        super();
    }
}

export class AuthLoginAttempt extends AuthAction {
    readonly type = AuthActionTypes.LoginAttempt;

    constructor(public readonly username: string,
                public readonly password: string,
                public readonly staySignedIn: boolean) {
        super();
    }
}

export class AuthLoginSucceed extends AuthAction {
    readonly type = AuthActionTypes.LoginSucceed;
}

export class AuthLoginFail extends AuthAction {
    readonly type = AuthActionTypes.LoginFail;

    constructor(public readonly error: string) {
        super();
    }
}

export class AuthLogout extends AuthAction {
    readonly type = AuthActionTypes.Logout;
}
