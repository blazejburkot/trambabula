import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {of} from 'rxjs';
import {
    AuthAction,
    AuthActionTypes,
    AuthLoadingPrincipalFailed,
    AuthLoginAttempt,
    AuthLoginFail,
    AuthLoginSucceed,
    AuthPrincipalLoaded
} from './auth.actions';
import {AuthService} from './auth.service';
import {catchError, flatMap, map, switchMap, tap} from 'rxjs/operators';
import {BrowserCookiesService} from '@ngx-utils/cookies/browser';
import {Router} from '@angular/router';

@Injectable({providedIn: 'root'})
export class AuthEffects {

    @Effect()
    tryLoadPrincipal = this.actions
        .pipe(
            ofType<AuthAction>(AuthActionTypes.LoginSucceed, AuthActionTypes.TryLoadPrincipal),
            switchMap(_ =>
                this.authService.loadPrincipal()
                    .pipe(
                        map(user => new AuthPrincipalLoaded(user)),
                        catchError(error => of(new AuthLoadingPrincipalFailed(error)))
                    )
            )
        );


    @Effect({dispatch: false})
    redirectAfterAuthLoadedPrincipal = this.actions
        .pipe(
            ofType<AuthAction>(AuthActionTypes.LoadedPrincipal),
            tap(_ => {
                if (this.router.url === '/login') {
                    this.router.navigate(['/']);
                }
            })
        );


    @Effect()
    loginAttemptCredentials = this.actions
        .pipe(
            ofType<AuthAction>(AuthActionTypes.LoginAttempt),
            switchMap((loginAttempt: AuthLoginAttempt) =>
                this.authService.login(loginAttempt.username, loginAttempt.password, loginAttempt.staySignedIn)
                    .pipe(
                        map(_ => new AuthLoginSucceed()),
                        catchError(error => of(new AuthLoginFail(error)))
                    )
            )
        );


    @Effect({dispatch: false})
    logout = this.actions
        .pipe(
            ofType<AuthAction>(AuthActionTypes.Logout),
            flatMap(_ => this.authService.logout()),
            tap(_ => this.router.navigate(['/']))
        );


    constructor(private actions: Actions,
                private authService: AuthService,
                private cookies: BrowserCookiesService,
                private router: Router) {
    }
}
