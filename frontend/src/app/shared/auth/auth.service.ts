import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from '../model/users.model';
import {Observable} from 'rxjs';
import {UsersService} from '../../feature/users/users.service';

@Injectable({providedIn: 'root'})
export class AuthService {
    constructor(private httpClient: HttpClient,
                private usersService: UsersService) {
    }

    login(username: string, password: string, staySignedIn: boolean = false): Observable<any> {
        const authUrl = staySignedIn ? '/api/v1/login?trusted=true' : '/api/v1/login';
        const formData = new FormData();
        formData.append('username', username);
        formData.append('password', password);

        return this.httpClient.post<any>(authUrl, formData);
    }

    logout(): Observable<any> {
        return this.httpClient.post<any>('/api/v1/logout', new FormData());
    }

    loadPrincipal(): Observable<User> {
        return this.usersService.getUserById('me');
    }
}
