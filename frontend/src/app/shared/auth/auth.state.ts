import {User, UserRole} from '../model/users.model';

export class PrincipalState {
    constructor(public readonly user?: User) {
    }

    public static authenticated(user: User): PrincipalState {
        return new PrincipalState(user);
    }

    public static anonymous(): PrincipalState {
        return new PrincipalState();
    }

    public isAuthenticated(): boolean {
        return this.user !== undefined;
    }

    public isStandardUser(): boolean {
        return this.isAuthenticated();
    }

    public isAdmin(): boolean {
        return this.isAuthenticated() && this.user.role === UserRole.ADMIN;
    }
}

export class LoginState {
    constructor(public readonly succeed: boolean,
                public readonly numberOfAttempts: number = 0) {
    }
}
