import {LoginState, PrincipalState} from './auth.state';
import {AuthAction, AuthActionTypes, AuthLoginSucceed, AuthPrincipalLoaded} from './auth.actions';

export function principalReducer(state: PrincipalState = PrincipalState.anonymous(),
                                 action: AuthAction): PrincipalState {
    if (action instanceof AuthPrincipalLoaded) {
        return PrincipalState.authenticated(action.user); // TODO secretAccessToken will be provided in Issue #7
    }

    if (action.type === AuthActionTypes.LoginFail
        || action.type === AuthActionTypes.Logout) {
        return PrincipalState.anonymous();
    }

    return state;
}

export function loginStateReducer(state: LoginState = new LoginState(false),
                                  action: AuthAction): LoginState {
    if (action instanceof AuthLoginSucceed) {
        return new LoginState(true);
    }

    if (action.type === AuthActionTypes.LoginFail) {
        return new LoginState(false, state.numberOfAttempts + 1);
    }

    if (action.type === AuthActionTypes.Logout) {
        return new LoginState(false, 0);
    }

    return state;
}
