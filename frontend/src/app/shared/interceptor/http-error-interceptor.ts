import {Observable, throwError} from 'rxjs';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {catchError} from 'rxjs/operators';

export class HttpErrorInterceptor implements HttpInterceptor {

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(req).pipe(
            catchError((error: HttpErrorResponse) => {
                console.log(error);

                return throwError(error.message);

                // if (error.error instanceof ErrorEvent) {
                //     // A client-side (or network) error occurred
                //     return throwError(error.error);
                // } else {
                //     // The backend returned an unsuccessful response code
                //     return throwError(error.error.error);
                // }
            })
        );
    }
}
