import {ErrorHandler, Injectable} from '@angular/core';
import {LoggerService} from '../logger/logger.service';

@Injectable({providedIn: 'root'})
export class GlobalErrorHandler implements ErrorHandler {

    constructor(private logger: LoggerService) {
        this.logger.info('GlobalErrorHandler initialised');
    }

    handleError(error) {
        this.logger.error('GlobalErrorHandler: ', error);

        window.alert(error);
    }
}
