export class UserRole {
    static readonly USER = new UserRole('USER', 'user.role.user');
    static readonly ADMIN = new UserRole('ADMIN', 'user.role.admin');

    private constructor(public readonly id: string, public readonly labelKey: string) {
    }

    public static fromString(value: string): UserRole {
        let result = null;
        if (value) {
            const valueUpperCase = value.toUpperCase();
            result = this.all().find(type => type.id === valueUpperCase);
        }
        return result ? result : null;
    }

    public static all(): UserRole[] {
        return [this.USER, this.ADMIN];
    }
}

export interface UserBaseInfo {
    id?: string;
    firstName?: string;
    lastName?: string;
    defaultDisplayName?: string;
}

export interface User extends UserBaseInfo {
    email?: string;
    role?: UserRole;
    password?: string;
    version?: number;
    lastModificationDate?: string;
}

export interface RegistrationForm {
    email: string;
    password: string;
    firstName?: string;
    lastName?: string;
}
