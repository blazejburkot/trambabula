import {HttpParams} from '@angular/common/http';
import {FormControl} from '@angular/forms';

export class SearchRequest {
    query = new FormControl('');
    pageSize = 25;
    pageNumber = 0;

    toHttpParams(): HttpParams {
        return new HttpParams()
            .append('query', this.query.value)
            .append('page', this.pageNumber.toString())
            .append('size', this.pageSize.toString());
    }
}

export interface ResponsePage<T> {
    number: number;
    totalPages: number;
    totalElements: number;
    content: T[];
}
