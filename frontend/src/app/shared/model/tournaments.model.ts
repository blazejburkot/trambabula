export class TournamentSystem {
    static readonly TRAMBAMBULA = new TournamentSystem('TRAMBAMBULA', 'tournament.system.trambambula.name',
        'tournament.system.trambambula.dsc', []);

    private constructor(public readonly id: string, public readonly labelKey: string, public readonly tooltipKey: string,
                        public readonly enabledForTypes: string[]) {
    }

    public static all(): TournamentSystem[] {
        return [this.TRAMBAMBULA];
    }

    public static getForType(typeId: string): TournamentSystem[] {
        return this.all().filter((element) => element.enabledForTypes.indexOf(typeId) >= 0);
    }
}

export class TournamentType {
    static readonly INDIVIDUAL = new TournamentType('INDIVIDUAL', 'tournament.type.individual', [TournamentSystem.TRAMBAMBULA]);
    static readonly GROUP = new TournamentType('GROUP', 'tournament.type.group', []);

    private constructor(public readonly id: string,
                        public readonly labelKey: any,
                        public readonly systems: TournamentSystem[]) {
    }

    public static all(): TournamentType[] {
        return [this.INDIVIDUAL, this.GROUP];
    }
}

export class TournamentVisibilityOption {
    static readonly PUBLIC =
        new TournamentVisibilityOption('PUBLIC', 'tournament.visibility.public.name', 'tournament.visibility.public.dsc');
    static readonly PUBLIC_LINK =
        new TournamentVisibilityOption('PUBLIC_URL', 'tournament.visibility.publicLink.name', 'tournament.visibility.publicLink.dsc');
    static readonly PRIVATE =
        new TournamentVisibilityOption('PRIVATE', 'tournament.visibility.private.name', 'tournament.visibility.private.dsc');

    private constructor(public readonly id: string, public readonly labelKey: string, public readonly tooltipKey: string) {
    }

    public static all(): TournamentVisibilityOption[] {
        return [this.PUBLIC, this.PUBLIC_LINK, this.PRIVATE];
    }
}

export class TournamentStatus {
    static readonly PENDING = new TournamentStatus('PENDING', 'tournament.status.pending');
    static readonly IN_PROGRESS = new TournamentStatus('IN_PROGRESS', 'tournament.status.in-progress');
    static readonly COMPLETED = new TournamentStatus('COMPLETED', 'tournament.status.completed');

    private constructor(public readonly id: string, public readonly labelKey: string) {
    }

    public static all(): TournamentStatus[] {
        return [this.PENDING, this.IN_PROGRESS, this.COMPLETED];
    }
}

export interface TournamentParticipant {
    participantId?: string;
    displayName: string;
    userId?: string;
    firstName?: string;
    lastName?: string;
}

export interface Tournament {
    id: string;
    name: string;
}

export interface CreateTournamentRequest {
    name?: string;
    url?: string;
    type?: string;
    system?: string;
    visibility?: string;
    participants?: TournamentParticipant[];
}
