import {AbstractControl, FormGroup} from '@angular/forms';

export class FormUtils {
    public static getDirtyValues(form: FormGroup): object {
        const dirtyValues = {};
        Object.keys(form.controls)
            .forEach(key => {
                const currentControl: AbstractControl = form.controls[key];

                if (currentControl.dirty) {
                    // dirtyValues[key] = currentControl.controls
                    //     ? this.getDirtyValues(currentControl)
                    //     : currentControl.value;
                    dirtyValues[key] = currentControl.value;
                }
            });
        return dirtyValues;
    }

    public static getAllValues(form: FormGroup): object {
        const values = {};
        Object.keys(form.controls)
            .forEach(key => {
                const currentControl: AbstractControl = form.controls[key];
                values[key] = currentControl.value;
            });
        return values;
    }
}
