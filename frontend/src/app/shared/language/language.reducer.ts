import {LanguageAction, LanguageActionTypes} from './language.actions';
import {LanguageState} from './language.state';

export function languageReducer(state: LanguageState = LanguageState.initialState(),
                                action: LanguageAction): LanguageState {
    if (action.type === LanguageActionTypes.Change) {
        return new LanguageState(action.language);
    }
    return state;
}
