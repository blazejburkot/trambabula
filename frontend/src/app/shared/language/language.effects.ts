import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {of} from 'rxjs';
import {
    LanguageAction,
    LanguageActionTypes,
    LanguageChangeAction,
    LanguageChangeFailAction,
    LanguageChangeSucceedAction
} from './language.actions';
import {TranslateService} from '@ngx-translate/core';
import {BrowserCookiesService} from '@ngx-utils/cookies/browser';
import {catchError, exhaustMap, map} from 'rxjs/operators';
import {LanguageState} from './language.state';

@Injectable({providedIn: 'root'})
export class LanguageEffects {

    @Effect()
    setInitialLanguage = this.actions
        .pipe(
            ofType<LanguageChangeAction>(LanguageActionTypes.SetInitialLanguage),
            map(_ => {
                const langCookieValue = this.cookies.get(LanguageState.cookieName);
                const browserLang = this.translateService.getBrowserLang();
                const langValue = [langCookieValue, browserLang, LanguageState.defaultLanguage] // order by importance
                    .find(lang => LanguageState.availableLanguages.includes(lang));
                return new LanguageChangeAction(langValue);
            }),
            catchError(err => {
                console.error(err);
                return of(new LanguageChangeAction(LanguageState.defaultLanguage));
            })
        );


    @Effect()
    changeLanguage = this.actions
        .pipe(
            ofType<LanguageAction>(LanguageActionTypes.Change),
            exhaustMap((langAction: LanguageAction) =>
                this.translateService
                    .use(langAction.language)
                    .pipe(
                        map(() => {
                            this.cookies.put(LanguageState.cookieName, langAction.language, {httpOnly: true});
                            return new LanguageChangeSucceedAction(langAction.language);
                        }),
                        catchError(error => of(new LanguageChangeFailAction(langAction.language, error)))
                    )
            )
        );

    constructor(private actions: Actions,
                private translateService: TranslateService,
                private cookies: BrowserCookiesService) {
        translateService.setDefaultLang(LanguageState.defaultLanguage);
    }
}
