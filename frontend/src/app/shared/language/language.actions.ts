import {Action} from '@ngrx/store';

export enum LanguageActionTypes {
    SetInitialLanguage = '[Language] Set initial language',
    Change = '[Language] Change',
    ChangeSucceed = '[Language] Change - Succeed',
    ChangeFail = '[Language] Change - Fail',
}

export abstract class LanguageAction implements Action {
    readonly type;

    constructor(public language?: string) {
    }
}

export class LanguageSetInitialAction extends LanguageAction {
    readonly type = LanguageActionTypes.SetInitialLanguage;
}

export class LanguageChangeAction extends LanguageAction {
    readonly type = LanguageActionTypes.Change;
}

export class LanguageChangeSucceedAction extends LanguageAction {
    readonly type = LanguageActionTypes.ChangeSucceed;
}

export class LanguageChangeFailAction extends LanguageAction {
    readonly type = LanguageActionTypes.ChangeFail;

    constructor(language: string, error: any) {
        super(language);
        console.error(error);
    }
}
