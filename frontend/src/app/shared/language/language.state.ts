export class LanguageState {
    public static readonly availableLanguages = ['en', 'pl'];
    public static readonly defaultLanguage = 'en';
    public static readonly cookieName = 'usr_lang';

    constructor(public readonly language: string) {
    }

    public static initialState(): LanguageState {
        return new LanguageState(LanguageState.defaultLanguage);
    }
}
