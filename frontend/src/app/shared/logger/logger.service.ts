import {Injectable} from '@angular/core';

@Injectable({providedIn: 'root'})
export class LoggerService {

    public info: any;
    public warn: any;
    public error: any;
}
