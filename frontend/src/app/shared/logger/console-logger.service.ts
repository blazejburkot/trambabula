import {Injectable} from '@angular/core';
import {LoggerService} from './logger.service';

const noop = (): any => undefined;

@Injectable({providedIn: 'root'})
export class ConsoleLoggerService extends LoggerService {
    private static isDebugEnabled = true;

    constructor() {
        super();

        if (ConsoleLoggerService.isDebugEnabled) {
            console.log('Console Logger Service registered');
        }
    }

    get info() {
        if (ConsoleLoggerService.isDebugEnabled) {
            return console.log.bind(console);
        } else {
            return noop;
        }
    }

    get warn() {
        if (ConsoleLoggerService.isDebugEnabled) {
            return console.warn.bind(console);
        } else {
            return noop;
        }
    }

    get error() {
        if (ConsoleLoggerService.isDebugEnabled) {
            return console.error.bind(console);
        } else {
            return noop;
        }
    }
}
