import {LanguageState} from './shared/language/language.state';
import {ActionReducerMap, createFeatureSelector, createSelector} from '@ngrx/store';
import {languageReducer} from './shared/language/language.reducer';
import {LoginState, PrincipalState} from './shared/auth/auth.state';
import {loginStateReducer, principalReducer} from './shared/auth/auth.reducer';
import {registrationReducer, RegistrationState} from './feature/register/register.store';

export interface AppState {
    language: LanguageState;
    principal: PrincipalState;
    loginState: LoginState;
    registrationState: RegistrationState;
}

export const appStateReducers: ActionReducerMap<AppState> = {
    language: languageReducer,
    principal: principalReducer,
    loginState: loginStateReducer,
    registrationState: registrationReducer,
};

export class AppSelectors {
    public static languageStateSelector = createFeatureSelector<LanguageState>('language');

    public static principalStateSelector = createFeatureSelector<PrincipalState>('principal');
    public static principalIdSelector =
        createSelector(AppSelectors.principalStateSelector, st => st && st.isAuthenticated() ? st.user.id : undefined);

    public static loginStateSelector = createFeatureSelector<LoginState>('loginState');
    public static registrationStateSelector = createFeatureSelector<RegistrationState>('registrationState');
}
