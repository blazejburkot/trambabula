import {Injectable} from '@angular/core';
import {ResponsePage, SearchRequest} from '../../shared/model/search.model';
import {RegistrationForm, User, UserBaseInfo, UserRole} from '../../shared/model/users.model';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({providedIn: 'root'})
export class UsersService {
    constructor(private httpClient: HttpClient) {
    }

    search(request: SearchRequest): Observable<ResponsePage<User>> {
        const httpParams = request.toHttpParams();
        return this.httpClient.get<ResponsePage<User>>('/api/v1/users', {params: httpParams})
            .pipe(map(response => {
                response.content.forEach(this.fixUserRole);
                return response;
            }));
    }

    searchByUserName(userName: string): Observable<ResponsePage<UserBaseInfo>> {
        const httpParams: HttpParams = new HttpParams().append('userName', userName);
        return this.httpClient.get<ResponsePage<UserBaseInfo>>('/api/v1/users', {params: httpParams});
    }

    getUserById(id): Observable<User> {
        return this.httpClient.get<User>(`/api/v1/users/${id}`)
            .pipe(map(this.fixUserRole));
    }

    update(id, userUpdatedValues: User): void {
        this.httpClient.patch(`/api/v1/users/${id}`, userUpdatedValues)
            .subscribe(_ => {});
    }

    changeUserPassword(id, passwordValue: string) {
        this.httpClient.put(`/api/v1/users/${id}/password`, passwordValue).subscribe(_ => {});
    }

    registerUser(registrationForm: RegistrationForm): Observable<any> {
        return this.httpClient.post('/api/v1/users', registrationForm);
    }

    private fixUserRole(user: User): User {
        user.role = UserRole.fromString(user.role.toString());
        return user;
    }
}
