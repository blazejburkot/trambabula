import {Component, OnInit} from '@angular/core';
import {UserBaseInfo} from '../../../shared/model/users.model';
import {UsersService} from '../users.service';
import {FormControl} from '@angular/forms';
import {debounceTime, startWith, switchMap} from 'rxjs/operators';
import {MatDialogRef} from '@angular/material/dialog';

@Component({
    selector: 'app-select-user-dialog',
    templateUrl: 'select-user-dialog.component.html',
    styleUrls: ['select-user-dialog.component.scss']
})
export class SelectUserDialogComponent implements OnInit {
    queryControl = new FormControl('');
    matchingUsers: UserBaseInfo[];
    totalNumberOfMatchingUsers: number;

    constructor(private dialogRef: MatDialogRef<SelectUserDialogComponent>,
                private service: UsersService) {
    }

    ngOnInit(): void {
        this.queryControl.valueChanges
            .pipe(
                startWith(''),
                debounceTime(300),
                switchMap(value => this.service.searchByUserName(value))
            )
            .subscribe(response => {
                console.log(response);
                this.matchingUsers = response.content;
                this.totalNumberOfMatchingUsers = response.totalElements;
            });
    }

    onEnterKeyUp() {
        if (this.matchingUsers.length === 1) {
            this.dialogRef.close(this.matchingUsers[0]);
        }
    }
}
