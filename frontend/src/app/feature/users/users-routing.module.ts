import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UsersListComponent} from './list/users-list.component';
import {EditUserComponent} from './edit/edit-user.component';

const usersRoutes: Routes = [
    {path: 'users', component: UsersListComponent},
    {path: 'users/:id/edit', component: EditUserComponent},
];

@NgModule({
    imports: [RouterModule.forRoot(usersRoutes)],
    exports: [RouterModule]
})
export class UsersRoutingModule {
}
