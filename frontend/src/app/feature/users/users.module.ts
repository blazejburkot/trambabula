import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UsersListComponent} from './list/users-list.component';
import {EditUserComponent} from './edit/edit-user.component';
import {UsersRoutingModule} from './users-routing.module';
import {MatTableModule} from '@angular/material/table';
import {TranslateModule} from '@ngx-translate/core';
import {MatButtonModule} from '@angular/material/button';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatIconModule} from '@angular/material/icon';
import {MatCardModule} from '@angular/material/card';
import {FlexModule} from '@angular/flex-layout';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {ReactiveFormsModule} from '@angular/forms';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {SelectUserDialogComponent} from './select/select-user-dialog.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatListModule} from '@angular/material/list';


@NgModule({
    declarations: [UsersListComponent, EditUserComponent, SelectUserDialogComponent],
    exports: [SelectUserDialogComponent],
    entryComponents: [SelectUserDialogComponent],
    imports: [
        UsersRoutingModule,
        CommonModule,
        MatTableModule,
        TranslateModule,
        MatButtonModule,
        MatTooltipModule,
        MatIconModule,
        MatCardModule,
        FlexModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        ReactiveFormsModule,
        MatProgressSpinnerModule,
        MatDialogModule,
        MatListModule
    ]
})
export class UsersModule {
}
