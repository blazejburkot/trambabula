import {Component, OnInit} from '@angular/core';
import {User, UserRole} from '../../../shared/model/users.model';
import {TranslatePipe} from '@ngx-translate/core';
import {ResponsePage, SearchRequest} from '../../../shared/model/search.model';
import {UsersService} from '../users.service';

export interface ColumnDef {
    name: string;
    labelKey: string;
    sortable: boolean;
}

@Component({
    selector: 'app-users-list',
    templateUrl: './users-list.component.html',
    styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {
    userDataDef: ColumnDef[] = [
        {name: 'email', labelKey: 'user.email', sortable: true},
        {name: 'firstName', labelKey: 'user.firstName', sortable: true},
        {name: 'lastName', labelKey: 'user.lastName', sortable: true},
        {name: 'role', labelKey: 'page.users.list.role', sortable: true}
    ];
    usersColumns = ['index', 'email', 'firstName', 'lastName', 'role', 'edit'];
    users: User[];

    searchRequest: SearchRequest = new SearchRequest();

    constructor(private translatePipe: TranslatePipe, private service: UsersService) {
    }

    ngOnInit() {
        this.search();
    }

    search() {
        this.service.search(this.searchRequest)
            .subscribe((response: ResponsePage<User>) => {
                this.users = response.content;
            });
    }

    getCellValue(user: User, column: string) {
        const colValue = user[column];
        if (this.isEmpty(colValue)) {
            return '';
        }

        if (column === 'role') {
            const userRole: UserRole = colValue;
            return this.translatePipe.transform(userRole.labelKey);
        }

        return colValue;
    }

    private isEmpty(str: string): boolean {
        return (!str || 0 === str.length);
    }
}
