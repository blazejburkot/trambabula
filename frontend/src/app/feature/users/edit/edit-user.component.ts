import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {User, UserRole} from '../../../shared/model/users.model';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {UsersService} from '../users.service';
import {TranslatePipe} from '@ngx-translate/core';
import {FormUtils} from '../../../shared/util/form-utils';

@Component({
    selector: 'app-edit-user',
    templateUrl: './edit-user.component.html',
    styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {
    userId: string;

    allUserRoles: UserRole[] = UserRole.all();

    hideUserPassword = true;
    passwordForm = new FormControl('');
    userForm: FormGroup;

    constructor(private route: ActivatedRoute,
                private formBuilder: FormBuilder,
                private service: UsersService,
                private translatePipe: TranslatePipe) {
        this.userId = route.snapshot.paramMap.get('id');
    }

    ngOnInit() {
        this.service.getUserById(this.userId).subscribe(user => {
            const controlsConfig = {password: ['']};
            Object.keys(user).forEach(fieldName => controlsConfig[fieldName] = [user[fieldName]]);

            this.userForm = this.formBuilder.group(controlsConfig);
            this.userForm.get('email').setValidators([Validators.required, Validators.email]);
            this.userForm.get('role').setValidators([Validators.required]);
        });
    }

    onSubmit() {
        if (this.userForm.invalid) {
            window.alert(this.translatePipe.transform('fixInvalidForm'));
            return;
        }

        if (this.userForm.dirty) {
            const userUpdatedValues: User = FormUtils.getDirtyValues(this.userForm);
            userUpdatedValues.version = this.userForm.get('version').value;
            this.service.update(this.userId, userUpdatedValues);
        }

        const passwordValue = this.passwordForm.value.toString().trim();
        if (this.passwordForm.dirty && passwordValue !== '') {
            this.service.changeUserPassword(this.userId, passwordValue);
        }
    }
}
