import {Component} from '@angular/core';
import {Tournament} from '../../../shared/model/tournaments.model';
import {AppSelectors, AppState} from '../../../app.state';
import {PrincipalState} from '../../../shared/auth/auth.state';
import {select, Store} from '@ngrx/store';

@Component({
    selector: 'app-tournaments-list',
    templateUrl: './tournaments-list.component.html',
    styleUrls: ['./tournaments-list.component.scss']
})
export class TournamentsListComponent {
    principal: PrincipalState;

    tournamentsTblColumns: string[] = ['index', 'name', 'action'];
    tournaments: Tournament[] = [];

    constructor(private store: Store<AppState>) {
        this.store
            .pipe(select(AppSelectors.principalStateSelector))
            .subscribe(principal => this.principal = principal);
    }
}
