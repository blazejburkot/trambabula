import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CreateTournamentComponent} from './create/create-tournament.component';
import {TournamentDetailsComponent} from './details/tournament-details.component';
import {TournamentsRoutingModule} from './tournaments-routing.module';
import {TranslateModule} from '@ngx-translate/core';
import {MatButtonModule} from '@angular/material/button';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatCardModule} from '@angular/material/card';
import {FlexModule} from '@angular/flex-layout';
import {MatTableModule} from '@angular/material/table';
import {MatIconModule} from '@angular/material/icon';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {ReactiveFormsModule} from '@angular/forms';
import {MatRadioModule} from '@angular/material/radio';
import {TournamentsListComponent} from './list/tournaments-list.component';

@NgModule({
    declarations: [CreateTournamentComponent, TournamentDetailsComponent, TournamentsListComponent],
    imports: [
        CommonModule,
        TournamentsRoutingModule,
        TranslateModule,
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatTooltipModule,
        MatCardModule,
        FlexModule,
        MatTableModule,
        MatIconModule,
        MatAutocompleteModule,
        ReactiveFormsModule,
        MatRadioModule
    ]
})
export class TournamentsModule {
}
