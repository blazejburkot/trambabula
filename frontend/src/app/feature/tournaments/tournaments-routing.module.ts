import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CreateTournamentComponent} from './create/create-tournament.component';
import {TournamentDetailsComponent} from './details/tournament-details.component';
import {TournamentsListComponent} from './list/tournaments-list.component';

const tournamentRoutes: Routes = [
    {path: 'tournaments', component: TournamentsListComponent},
    {path: 'tournaments/create', component: CreateTournamentComponent},
    {path: 'tournaments/:id', component: TournamentDetailsComponent},
];

@NgModule({
    imports: [RouterModule.forRoot(tournamentRoutes)],
    exports: [RouterModule]
})
export class TournamentsRoutingModule {
}
