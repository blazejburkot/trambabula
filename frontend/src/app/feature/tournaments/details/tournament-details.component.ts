import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'app-tournament-details',
    templateUrl: './tournament-details.component.html',
    styleUrls: ['./tournament-details.component.scss']
})
export class TournamentDetailsComponent implements OnInit {
    tournamentId: string;

    constructor(private route: ActivatedRoute) {
        this.tournamentId = route.snapshot.paramMap.get('id');
    }

    ngOnInit() {
    }

}
