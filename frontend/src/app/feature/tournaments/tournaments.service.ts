import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CreateTournamentRequest} from '../../shared/model/tournaments.model';
import {Observable} from 'rxjs';

@Injectable({providedIn: 'root'})
export class TournamentsService {
    constructor(private httpClient: HttpClient) {
    }

    createTournament(request: CreateTournamentRequest): Observable<any> {
        console.log(request);
        return this.httpClient.post('/api/v1/tournaments', request);
    }
}
