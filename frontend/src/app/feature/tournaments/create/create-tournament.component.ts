import {Component} from '@angular/core';
import {
    CreateTournamentRequest,
    TournamentParticipant,
    TournamentSystem,
    TournamentType,
    TournamentVisibilityOption
} from '../../../shared/model/tournaments.model';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {FormUtils} from '../../../shared/util/form-utils';
import {TournamentsService} from '../tournaments.service';
import {UserBaseInfo} from '../../../shared/model/users.model';
import {MatDialog} from '@angular/material/dialog';
import {SelectUserDialogComponent} from '../../users/select/select-user-dialog.component';
import {MatTableDataSource} from '@angular/material/table';
import {AppStateService} from '../../../app-state.service';

@Component({
    selector: 'app-create-tournament',
    templateUrl: './create-tournament.component.html',
    styleUrls: ['./create-tournament.component.scss']
})
export class CreateTournamentComponent {
    types: TournamentType[] = TournamentType.all();
    systems: TournamentSystem[] = [];
    visibilityOptions: TournamentVisibilityOption[] = TournamentVisibilityOption.all();

    newParticipant: TournamentParticipant = {displayName: ''};

    tblParticipantsColumns: string[] = ['index', 'displayName', 'name', 'action'];
    participants: MatTableDataSource<TournamentParticipant> = new MatTableDataSource<TournamentParticipant>();

    errorLabelKey: string;

    createTournamentForm: FormGroup = this.formBuilder.group({
        name: ['', [Validators.required]],
        url: [''],
        type: [null, [Validators.required]],
        system: [null, [Validators.required]],
        visibility: [null, [Validators.required]]
    });


    constructor(private appStateService: AppStateService,
                private formBuilder: FormBuilder,
                private service: TournamentsService,
                private dialog: MatDialog) {
        this.appStateService.redirectToHomeIfUserIsNotAuthenticated();

        this.participants.data = [];
    }


    onSubmit() {
        if (this.createTournamentForm.invalid) {
            this.appStateService.showErrorMessage('fixInvalidForm');
            return;
        }

        const request: CreateTournamentRequest = FormUtils.getAllValues(this.createTournamentForm);
        request.participants = this.participants.data;
        this.service.createTournament(request)
            .subscribe(tournamentId => this.appStateService.navigateTo(`/tournaments/${tournamentId}`));
    }

    addParticipant() {
        const displayName = this.newParticipant.displayName;
        if (displayName.length === 0
            || this.participants.data.filter(p => this.equalsIgnoreCase(p.displayName, displayName)).length > 0) {
            this.appStateService.showErrorMessage('page.tournament.create.error.wrongDisplayName');
            return;
        }

        this.participants.data = this.participants.data.concat(this.newParticipant);
        this.newParticipant = {displayName: ''};
    }

    removeParticipant(participant: any) {
        this.participants.data = this.participants.data.filter(obj => obj !== participant);
    }

    openSelectUserDialog() {
        const dialogRef = this.dialog.open(SelectUserDialogComponent, {minHeight: '400px'});

        dialogRef.afterClosed().subscribe((selectedUser: UserBaseInfo) => {
            console.log('The dialog was closed', selectedUser);
            if (selectedUser) {
                this.newParticipant.userId = selectedUser.id;
                this.newParticipant.firstName = selectedUser.firstName;
                this.newParticipant.lastName = selectedUser.lastName;
                if (this.newParticipant.displayName.length === 0) {
                    this.newParticipant.displayName = selectedUser.defaultDisplayName;
                }
            }
        });
    }

    private equalsIgnoreCase(str1: string, str2: string): boolean {
        return str1.toLowerCase() === str2.toLowerCase();
    }
}
