import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Router} from '@angular/router';
import {Action} from '@ngrx/store';
import {catchError, exhaustMap, map, tap} from 'rxjs/operators';
import {UsersService} from '../users/users.service';
import {of} from 'rxjs';
import {RegistrationForm} from '../../shared/model/users.model';

export enum RegisterActionTypes {
    RegisterUser = '[Register] Register User',
    RegisterUserSucceed = '[Register] Register User Succeed',
    RegisterUserFail = '[Register] Register User Fail'
}

export abstract class RegisterAction implements Action {
    protected constructor(public readonly type) {
    }
}

export class RegisterUser extends RegisterAction {
    constructor(public readonly registrationForm: RegistrationForm) {
        super(RegisterActionTypes.RegisterUser);
    }
}

export class RegisterUserSucceed extends RegisterAction {
    constructor() {
        super(RegisterActionTypes.RegisterUserSucceed);
    }
}

export class RegisterUserFail extends RegisterAction {
    constructor(public readonly error: any) {
        super(RegisterActionTypes.RegisterUserFail);
    }
}

export class RegistrationState {
    constructor(public readonly succeed: boolean,
                public readonly errorMessageKey?: string) {
    }
}


export function registrationReducer(state: RegistrationState = new RegistrationState(false),
                                    action: RegisterAction): RegistrationState {
    if (action instanceof RegisterUserSucceed) {
        return new RegistrationState(true);
    }
    if (action instanceof RegisterUserFail) {
        return new RegistrationState(false, 'page.register.emailIsAlreadyRegister');
    }
    return state;
}

@Injectable({providedIn: 'root'})
export class RegisterEffects {

    @Effect()
    registerUser = this.actions
        .pipe(
            ofType<RegisterUser>(RegisterActionTypes.RegisterUser),
            exhaustMap(registerAction =>
                this.usersService.registerUser(registerAction.registrationForm)
                    .pipe(
                        map(_ => new RegisterUserSucceed()),
                        catchError(error => of(new RegisterUserFail(error)))
                    )
            )
        );

    @Effect({dispatch: false})
    redirectAfterSuccessfulRegistration = this.actions
        .pipe(
            ofType<RegisterUserSucceed>(RegisterActionTypes.RegisterUserSucceed),
            tap(_ => this.router.navigate(['/']))
        );

    constructor(private actions: Actions,
                private usersService: UsersService,
                private router: Router) {
    }
}
