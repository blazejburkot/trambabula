import {Component} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {select, Store} from '@ngrx/store';
import {AppSelectors, AppState} from '../../app.state';
import {FormUtils} from '../../shared/util/form-utils';
import {RegisterUser} from './register.store';
import {RegistrationForm} from '../../shared/model/users.model';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss']
})
export class RegisterComponent {
    registrationErrorLabelKey: string;
    hideUserPassword = true;
    userForm: FormGroup = this.formBuilder.group({
        email: ['', [Validators.required, Validators.email]],
        firstName: [''],
        lastName: [''],
        password: ['', [Validators.required]],
    });

    constructor(private store: Store<AppState>,
                private formBuilder: FormBuilder) {
        store
            .pipe(select(AppSelectors.registrationStateSelector))
            .subscribe(registrationState => {
                if (!registrationState.succeed && registrationState.errorMessageKey) {
                    this.registrationErrorLabelKey = registrationState.errorMessageKey;
                }
            });
    }

    onSubmit() {
        // @ts-ignore
        const registrationForm: RegistrationForm = FormUtils.getAllValues(this.userForm);
        this.store.dispatch(new RegisterUser(registrationForm));
    }
}
