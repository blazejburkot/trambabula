import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {select, Store} from '@ngrx/store';
import {AppSelectors, AppState} from '../../app.state';
import {AuthLoginAttempt, AuthLoginFail} from '../../shared/auth/auth.actions';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    credentialsForm: FormGroup;
    hideUserPassword = true;
    wrongCredentials = false;

    constructor(private store: Store<AppState>,
                private formBuilder: FormBuilder) {
        this.store
            .pipe(select(AppSelectors.loginStateSelector))
            .subscribe(loginState => {
                if (loginState.numberOfAttempts > 0) {
                    this.wrongCredentials = true;
                }
            });
    }

    ngOnInit() {
        this.credentialsForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', Validators.required],
            staySignedIn: [false]
        });
    }

    onSubmit() {
        const username = this.credentialsForm.get('email').value;
        const password = this.credentialsForm.get('password').value;
        const staySignedIn = this.credentialsForm.get('staySignedIn').value;

        if (username && password) {
            this.store.dispatch(new AuthLoginAttempt(username, password, staySignedIn));
        } else {
            this.store.dispatch(new AuthLoginFail('username or password is empty'));
        }
    }
}
