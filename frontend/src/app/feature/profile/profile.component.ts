import {Component, OnInit} from '@angular/core';
import {select, Store} from '@ngrx/store';
import {AppSelectors, AppState} from '../../app.state';
import {filter, flatMap} from 'rxjs/operators';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {User} from '../../shared/model/users.model';
import {UsersService} from '../users/users.service';
import {ActionFail} from '../../app-common-events';
import {FormUtils} from '../../shared/util/form-utils';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
    userId: string;
    userData: User;

    errorLabelKey: string;
    hideUserPassword = true;
    passwordForm = new FormControl('');
    userForm: FormGroup;

    constructor(private store: Store<AppState>,
                private formBuilder: FormBuilder,
                private usersService: UsersService) {
        this.store
            .pipe(
                select(AppSelectors.principalIdSelector),
                filter(userId => !!userId),
                flatMap(userId => usersService.getUserById(userId))
            )
            .subscribe(
                user => {
                    this.userId = user.id;
                    this.userData = user;
                    this.createForms(user);
                },
                err => store.dispatch(new ActionFail('profile.open-tab', err)));
    }

    ngOnInit() {
    }

    private createForms(user: User) {
        const controlsConfig = {password: ['']};
        Object.keys(user).forEach(fieldName => controlsConfig[fieldName] = [user[fieldName]]);

        this.userForm = this.formBuilder.group(controlsConfig);
        this.userForm.get('email').setValidators([Validators.required, Validators.email]);
        this.userForm.get('role').setValidators([Validators.required]);
    }

    onSubmit() {
        this.errorLabelKey = null;
        if (this.userForm.invalid) {
            this.errorLabelKey = 'fixInvalidForm';
            return;
        }

        if (this.userForm.dirty) {
            const userUpdatedValues: User = FormUtils.getDirtyValues(this.userForm);
            userUpdatedValues.version = this.userForm.get('version').value;
            this.usersService.update(this.userId, userUpdatedValues);
        }

        const passwordValue = this.passwordForm.value.toString().trim();
        if (this.passwordForm.dirty && passwordValue !== '') {
            this.usersService.changeUserPassword(this.userId, passwordValue);
        }
    }
}
