import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NavbarComponent} from './navbar.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {TranslateModule} from '@ngx-translate/core';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatButtonModule} from '@angular/material/button';
import {RouterModule} from '@angular/router';
import {MatIconModule} from '@angular/material/icon';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatMenuModule} from '@angular/material/menu';
import {MatSelectModule} from '@angular/material/select';


@NgModule({
    declarations: [NavbarComponent],
    exports: [NavbarComponent],
    imports: [
        CommonModule,
        MatToolbarModule,
        TranslateModule,
        FlexLayoutModule,
        MatButtonModule,
        RouterModule,
        MatIconModule,
        MatTooltipModule,
        MatMenuModule,
        MatSelectModule
    ]
})
export class NavbarModule {
}
