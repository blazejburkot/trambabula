import {Component, OnInit} from '@angular/core';
import {select, Store} from '@ngrx/store';
import {AppSelectors, AppState} from '../app.state';
import {PrincipalState} from '../shared/auth/auth.state';
import {AuthLogout} from '../shared/auth/auth.actions';
import {LanguageState} from '../shared/language/language.state';
import {LanguageChangeAction} from '../shared/language/language.actions';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
    principal: PrincipalState;

    selectedLang: string;
    availableLanguages = LanguageState.availableLanguages;

    constructor(private store: Store<AppState>) {
        this.store
            .pipe(select(AppSelectors.principalStateSelector))
            .subscribe(principal => this.principal = principal);

        this.store
            .pipe(select(AppSelectors.languageStateSelector))
            .subscribe(langState => this.selectedLang = langState.language);
    }

    ngOnInit() {
    }

    printUserName(): string {
        if (!this.principal || !this.principal.isAuthenticated()) {
            return '';
        }

        const user = this.principal.user;
        return user.firstName
            ? `${user.firstName} ${user.lastName}`
            : user.email;
    }

    logout() {
        this.store.dispatch(new AuthLogout());
    }

    onLanguageChange(newLanguage: string) {
        this.store.dispatch(new LanguageChangeAction(newLanguage));
    }
}
