import {Injectable} from '@angular/core';
import {select, Store} from '@ngrx/store';
import {AppSelectors, AppState} from './app.state';
import {Observable} from 'rxjs';
import {PrincipalState} from './shared/auth/auth.state';
import {filter} from 'rxjs/operators';
import {Router} from '@angular/router';
import {MatSnackBar} from '@angular/material/snack-bar';
import {TranslateService} from '@ngx-translate/core';

@Injectable({providedIn: 'root'})
export class AppStateService {

    constructor(private store: Store<AppState>,
                private router: Router,
                private translationService: TranslateService,
                private snackBarErrorMsg: MatSnackBar) {
    }

    getPrincipal(): Observable<PrincipalState> {
        return this.store.pipe(select(AppSelectors.principalStateSelector));
    }

    redirectToHomeIfUserIsNotAuthenticated(): void {
        this.getPrincipal()
            .pipe(filter(principal => !principal.isAuthenticated()))
            .subscribe(principal => {
                console.log('redirect', principal);
                this.router.navigate(['/']);
            });
    }

    navigateTo(path: string): void {
        this.router.navigate([path]);
    }

    showErrorMessage(messageKey: string) {
        const message = this.translationService.instant(messageKey);
        this.snackBarErrorMsg.open(message, '', {duration: 3500});
    }
}
