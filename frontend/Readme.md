# Trambambula - frontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.19.

## Development

### Prepare environment
1. Install node.js ([link](https://nodejs.org/en/download/))
1. Verify node.js installation
   > node -v  
     npm -v  
1. Install Angular CLI  
   > npm install -g @angular/cli@latest  
1. Verify Angular CLI installation
   > ng -v
1. Install TSLint  
   > npm install tslint typescript -g
1. Install dependencies
   > npm install
1. (Optional) Install browser extension from [the link](https://github.com/zalmoxisus/redux-devtools-extension/)
1. Run backend
   > run main method from class RunTestWebServer.java


### Commands
- Verify code -> `npm run validate` 
- Run dev server -> `ng serve --open`

- Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`. ([docs](https://angular.io/cli/generate))

### Links
- https://angular.io/
- https://cli.angular.io/
- https://material.angular.io/components/categories

### Guides
- https://blog.angular-university.io/rxjs-higher-order-mapping/

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
