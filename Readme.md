# Trambambula

TODO add description

## Overall information

Import `.editorconfig` ([doc](https://editorconfig.org/)) settings to your IDE before making any changes 

## Key technologies
[Backend](backend/Readme.md): Java, Spring and Maven

[Frontend](frontend/Readme.md): Node.js, Typescript and Angular
